﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Servicios
{
    public class EnvioService
    {
        public static bool EnviarEmail(string email, string url)
        {
            MailMessage msg = new MailMessage();
            msg.To.Add(email);
            msg.From = new MailAddress("experienciaturistica2015@gmail.com", "Experiencia-Turítica", System.Text.Encoding.UTF8);
            msg.Subject = "Completar acción de Experiencia Turística.";
            msg.SubjectEncoding = System.Text.Encoding.UTF8;
            msg.Body = "Por favor ingrese a siguiente enlace para concretar su acción: " + url;
            msg.BodyEncoding = System.Text.Encoding.UTF8;
            msg.IsBodyHtml = false;

            SmtpClient client = new SmtpClient();
            client.Credentials = new System.Net.NetworkCredential("experienciaturistica2015@gmail.com", "experiencia2015");
            client.Port = 587;
            client.Host = "smtp.gmail.com";
            client.EnableSsl = true;
            try
            {
                client.Send(msg);
                return true;
            }

            catch
            {
                return false;
            }
        }
    }
}
