﻿using Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RepositorioClases;

namespace Servicios
{
    public static class RolServicio
    {
        public static List<Roles> ObtenerRoles()
        {
            using (Entities context = new Entities())
            {
                List<Roles> roles = context.Roles.ToList();
                return roles;

            }

        }

        public static List<Rol> GetRole(int? id)
        {
            using (Entities context = new Entities())
            {
                // la propiedad HasValue de los objetos que son Nullables o permiten tomar valores nulos, equivale a hacer la pregunta if (valor != null)
                // la expresión id.HasValue ? id.Value == u.Id : true
                // equivale a (id.HasValue && id.Value == u.Id) || (!id.HasValue)
                // lo que hacen es filtrar solo cuando viene un valor en la variable, ahorrándonos hacer un if antes y repetir la consulta.
                return context.Roles.Where(r => id.HasValue ? id.Value == r.IdRol : true).Select(r => new Rol()
                {
                    DescripcionRol= r.DescripcionRol,
                    IdRol = r.IdRol
                }).ToList();
            }
        }

        public static Rol ObtenerRolPorIdRol(int? IdRol)
        {
            using (Entities context = new Entities())
            {
                Rol rol = context.Roles.Where(t => t.IdRol == IdRol).Select(x => new Rol()
                {
                    IdRol = x.IdRol,
                    DescripcionRol = x.DescripcionRol
                }).SingleOrDefault();
                return rol;
            }
        }
    }
}
