﻿using Datos;
using RepositorioClases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicios
{
    public class BusquedaService
    {
        public List<ResultadoBusqueda> BusquedaGeneral(string parametroBusqueda)
        {
            using (Entities context = new Entities())
            {
                List<ResultadoBusqueda> resultados = new List<ResultadoBusqueda>();
                List<Temas> busquedaTemas = context.Temas.Where(t => t.TituloTema.Contains(parametroBusqueda)
                                                        || t.DescripcionTema.Contains(parametroBusqueda)
                                                        || t.ContenidoTema.Contains(parametroBusqueda)).ToList();

                var cont = 0;
                foreach (var b in busquedaTemas)
                {
                    if (cont <= 3)
                    {
                        if (b.FechaEliminacion == null)
                        {
                            ResultadoBusqueda res = new ResultadoBusqueda();
                            res.Id = b.IdTema;
                            res.TituloRepresentativo = b.TituloTema;
                            res.TipoResultado = "Tema";
                            resultados.Add(res);
                        }
                        cont += 1;
                    }
                    else
                    {
                        cont = 0;
                        break;
                    }
                }

                List<Temas> busquedaCategorias = context.Temas.Where(t => t.CategoriasTemas.DescripcionCategoria.Contains(parametroBusqueda)).ToList();

                foreach (var b in busquedaCategorias)
                {
                    if (cont <= 3)
                    {
                        if (b.FechaEliminacion == null)
                        {
                            ResultadoBusqueda res = new ResultadoBusqueda();
                            res.Id = b.CategoriasTemas.IdCategoria;
                            res.TituloRepresentativo = b.CategoriasTemas.DescripcionCategoria;
                            res.TipoResultado = "Categoría";
                            resultados.Add(res);
                        }
                        cont += 1;
                    }
                    else
                    {
                        cont = 0;
                        break;
                    }
                }

                List<ComentariosTemas> busquedaComentarios = context.ComentariosTemas.Where(t => t.DescripcionComentario.Contains(parametroBusqueda)).ToList();
                foreach (var c in busquedaComentarios)
                {
                    if (cont <= 3)
                    {
                        if (c.FechaHoraEliminacion == null)
                        {
                            ResultadoBusqueda res = new ResultadoBusqueda();
                            res.Id = c.IdTema;
                            res.TituloRepresentativo = c.DescripcionComentario;
                            res.TipoResultado = "Comentario";
                            resultados.Add(res);
                        }
                        cont += 1;
                    }
                    else
                    {
                        cont = 0;
                        break;
                    }
                }

                //HACER GALERIA
                List<Galerias> busquedaGaleria = context.Galerias.Where(t => t.TituloGaleria.Contains(parametroBusqueda)
                                                                        || t.DescripcionGaleria.Contains(parametroBusqueda)).ToList();
                foreach (var d in busquedaGaleria)
                {
                    if (cont <= 3)
                    {
                        if (d.FechaEliminacion == null)
                        {
                            ResultadoBusqueda res = new ResultadoBusqueda();
                            res.Id = d.IdGaleria;
                            res.TituloRepresentativo = d.TituloGaleria;
                            res.TipoResultado = "Galeria";
                            resultados.Add(res);
                        }
                        cont += 1;
                    }
                    else
                    {
                        cont = 0;
                        break;
                    }
                }
                return resultados;
            }
        }

        public List<ResultadoBusqueda> BusquedaTema(string parametroBusqueda)
        {
            using (Entities context = new Entities())
            {
                List<ResultadoBusqueda> resultados = new List<ResultadoBusqueda>();
                List<Temas> busquedaTemas = context.Temas.Where(t => t.TituloTema.Contains(parametroBusqueda)
                                                        || t.DescripcionTema.Contains(parametroBusqueda)
                                                        || t.ContenidoTema.Contains(parametroBusqueda)).ToList();

                var cont = 0;
                foreach (var b in busquedaTemas)
                {
                    if (cont <= 3)
                    {
                        if (b.FechaEliminacion == null)
                        {
                            ResultadoBusqueda res = new ResultadoBusqueda();
                            res.Id = b.IdTema;
                            res.TituloRepresentativo = b.TituloTema;
                            res.TipoResultado = "Tema";
                            resultados.Add(res);
                        }
                        cont += 1;
                    }
                    else
                    {
                        cont = 0;
                        break;
                    }
                }

                List<ComentariosTemas> busquedaComentarios = context.ComentariosTemas.Where(t => t.DescripcionComentario.Contains(parametroBusqueda)).ToList();
                foreach (var c in busquedaComentarios)
                {
                    if (cont <= 3)
                    {
                        if (c.FechaHoraEliminacion == null)
                        {
                            ResultadoBusqueda res = new ResultadoBusqueda();
                            res.Id = c.IdTema;
                            res.TituloRepresentativo = c.DescripcionComentario;
                            res.TipoResultado = "Comentario";
                            resultados.Add(res);
                        }
                        cont += 1;
                    }
                    else
                    {
                        cont = 0;
                        break;
                    }
                }

                List<Temas> busquedaCategorias = context.Temas.Where(t => t.CategoriasTemas.DescripcionCategoria.Contains(parametroBusqueda)).ToList();

                foreach (var b in busquedaCategorias)
                {
                    if (cont <= 3)
                    {
                        if (b.FechaEliminacion == null)
                        {
                            ResultadoBusqueda res = new ResultadoBusqueda();
                            res.Id = b.CategoriasTemas.IdCategoria;
                            res.TituloRepresentativo = b.CategoriasTemas.DescripcionCategoria;
                            res.TipoResultado = "Categoría";
                            resultados.Add(res);
                        }
                        cont += 1;
                    }
                    else
                    {
                        cont = 0;
                        break;
                    }
                }
                return resultados;
            }
        }
        public List<ResultadoBusqueda> BusquedaGaleria(string parametroBusqueda)
        {
            using (Entities context = new Entities())
            {
                List<ResultadoBusqueda> resultadosGal = new List<ResultadoBusqueda>();
                List<Galerias> busquedaGalerias = context.Galerias.Where(t => t.TituloGaleria.Contains(parametroBusqueda)
                                                        || t.DescripcionGaleria.Contains(parametroBusqueda)).ToList();

                var cont = 0;
                foreach (var b in busquedaGalerias)
                {
                    if (cont <= 3)
                    {
                        if (b.FechaEliminacion == null)
                        {
                            ResultadoBusqueda res = new ResultadoBusqueda();
                            res.Id = b.IdGaleria;
                            res.TituloRepresentativo = b.TituloGaleria;
                            res.TipoResultado = "Galeria";
                            resultadosGal.Add(res);
                        }
                        cont += 1;
                    }
                    else
                    {
                        cont = 0;
                        break;
                    }
                }
                return resultadosGal;
            }
        }

        public List<ResultadoBusqueda> BusquedaAvanzada(string titulo, string copete, string descripcion, bool relacionada)
        {
            using (Entities context = new Entities())
            {
                List<ResultadoBusqueda> resultados = new List<ResultadoBusqueda>();
                List<Temas> busquedaTemas = new List<Temas>();
                if (relacionada == true)
                {
                    busquedaTemas = context.Temas.Where(t => t.TituloTema.Contains(titulo)
                                                            && t.DescripcionTema.Contains(copete)
                                                            && t.ContenidoTema.Contains(descripcion)).ToList();
                }
                else
                {
                    busquedaTemas = context.Temas.Where(t => t.TituloTema.Contains(titulo)
                                                            || t.DescripcionTema.Contains(copete)
                                                            || t.ContenidoTema.Contains(descripcion)).ToList();
                }

                foreach (var b in busquedaTemas)
                {
                    if (b.FechaEliminacion == null)
                    {
                        ResultadoBusqueda res = new ResultadoBusqueda();
                        res.Id = b.IdTema;
                        res.TituloRepresentativo = b.TituloTema;
                        res.TipoResultado = "Tema";
                        resultados.Add(res);
                    }
                }

                return resultados;
            }
        }
    }


}
