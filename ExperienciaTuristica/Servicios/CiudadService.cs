﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RepositorioClases;
using Datos;
using System.Linq.Expressions;

namespace Servicios
{
    public static class CiudadServicio
    {
        public static List<Ciudades> ObtenerCiudades()
        {
            using (Entities context = new Entities())
            {
                var c = context.Ciudades.OrderBy(x => x.CiudadNombre);
                List<Ciudades> ciudades = c.ToList();
                return ciudades;
            }

        }
        //public static List<Ciudades> FiltrarCiudades(Expression<Func<Ciudades, bool>> predicate)
        //{
        //    using (Entities context = new Entities())
        //    {
        //        List<Ciudades> ciudadesfilt = context.Set<Ciudades>().Where(predicate).ToList();
        //        return ciudadesfilt;
        //    }
        //}
        public static Ciudad ObtenerCiudadPorIdCiudad(int? IdCiudad)
        {
            using (Entities context = new Entities())
            {
                Ciudad ciu = context.Ciudades.Where(t => t.CiudadId == IdCiudad).Select(x => new Ciudad()
                {
                    CiudadId = x.CiudadId,
                    CiudadNombre = x.CiudadNombre
                }).SingleOrDefault();
                return ciu;
            }
        }


    }


}
