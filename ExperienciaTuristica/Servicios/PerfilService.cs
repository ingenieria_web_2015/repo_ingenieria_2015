﻿using Datos;
using RepositorioClases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicios
{
    public static class PerfilService
    {
        public static Usuario ObtenerUsuario(int? id)
        {
            using (Entities context = new Entities())
            {
                // la propiedad HasValue de los objetos que son Nullables o permiten tomar valores nulos, equivale a hacer la pregunta if (valor != null)
                // la expresión id.HasValue ? id.Value == u.Id : true
                // equivale a (id.HasValue && id.Value == u.Id) || (!id.HasValue)
                // lo que hacen es filtrar solo cuando viene un valor en la variable, ahorrándonos hacer un if antes y repetir la consulta.
                return context.Usuarios.Where(u => (id.HasValue ? id.Value == u.IdUsuario : true) && u.IdEstado == 1).Select(u => new Usuario()
                {
                    Contraseña = u.Contraseña,
                    Direccion = u.Direccion,
                    FechaNacimiento = u.FechaNacimiento,
                    IdEstado = u.IdEstado,
                    IdCiudad = u.IdCiudad,
                    IdRol = u.IdRol,
                    IdUsuario = u.IdUsuario,
                    Mail = u.Mail,
                    NombreUsuario = u.NombreUsuario,
                    NombreApellido = u.NombreApellido,
                    FotoPerfil = u.FotoPerfil
                }).FirstOrDefault();
            }
        }

        public static void GuardarPerfilUsuario(Usuario usuario, string filename)
        {
            using (Entities context = new Entities())
            {
                Usuarios users = context.Usuarios.Where(u => u.IdUsuario == usuario.IdUsuario).FirstOrDefault();

                if (users != null)
                {
                    users.Contraseña = usuario.Contraseña;
                    users.Direccion = usuario.Direccion;
                    users.FechaNacimiento = usuario.FechaNacimiento;
                    users.IdEstado = usuario.IdEstado;
                    users.IdCiudad = usuario.IdCiudad;
                    users.IdRol = usuario.IdRol;
                    users.IdUsuario = usuario.IdUsuario;
                    users.Mail = usuario.Mail;
                    users.NombreUsuario = usuario.NombreUsuario;
                    users.NombreApellido = usuario.NombreApellido;
                    users.FotoPerfil = filename;
                }

                context.SaveChanges();
            }
        }
        public static void EditarPerfil(Usuario usuario)
        {
            using (Entities context = new Entities())
            {
                Usuarios users = context.Usuarios.Where(u => u.IdUsuario == usuario.IdUsuario).FirstOrDefault();

                if (users != null)
                {
                    users.Direccion = usuario.Direccion;
                    users.FechaNacimiento = usuario.FechaNacimiento;
                    users.IdCiudad = usuario.IdCiudad;
                    users.IdUsuario = usuario.IdUsuario;
                    users.Mail = usuario.Mail;
                    users.NombreUsuario = usuario.NombreUsuario;
                    users.NombreApellido = usuario.NombreApellido;
                }

                context.SaveChanges();
            }
        }
    }
}

