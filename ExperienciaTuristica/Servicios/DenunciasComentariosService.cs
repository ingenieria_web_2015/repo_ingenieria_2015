﻿using Datos;
using RepositorioClases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicios
{
    public class DenunciasComentariosService
    {
        public static List<DenunciaComentario> ObtenerDenuncias(int? IdUsuario, int? IdComentario)
        {
            using (Entities context = new Entities())
            {

                List<DenunciasComentarios> listaDenuncias = context.DenunciasComentarios.Where(x => x.DenunciaRevisada == null).ToList();
                var grupos = listaDenuncias.GroupBy(x => x.IdComentario);
                List<DenunciaComentario> denunciascomentarios = new List<DenunciaComentario>();

                foreach (var grupo in grupos)
                {
                    if (grupo.Count() > 3)
                    {
                        DenunciaComentario dc = new DenunciaComentario();
                        dc.IdComentario = grupo.FirstOrDefault().IdComentario;
                        dc.FechaHoraDenuncia = grupo.FirstOrDefault().FechaHoraDenuncia;
                        List<DenunciaComentarioDetalle> detalle = new List<DenunciaComentarioDetalle>();
                        foreach (var valor in grupo)
                        {
                            DenunciaComentarioDetalle val = new DenunciaComentarioDetalle();
                            val.IdUsuario = valor.IdUsuario;
                            val.MotivoDenuncia = valor.MotivoDenuncia;
                            detalle.Add(val);
                        }
                        dc.Detalle = detalle;
                        denunciascomentarios.Add(dc);
                    }
                }

                return denunciascomentarios;
            }
        }

        public static List<DenunciasComentarios> ObtenerDenuncia(int? IdUsuario, int? IdComentario)
        {
            using (Entities context = new Entities())
            {
                List<DenunciasComentarios> listaDenuncias = context.DenunciasComentarios.Where(x => x.IdComentario == IdComentario && x.IdUsuario == IdUsuario).ToList();

                return listaDenuncias;
            }
        }

        public static DenunciaComentario ObtenerDenunciasPorIdComentario(int IdComentario)
        {
            using (Entities context = new Entities())
            {
                List<DenunciasComentarios> listaDenuncias = context.DenunciasComentarios.Where(x => x.IdComentario == IdComentario).ToList();
                var grupos = listaDenuncias.GroupBy(x => x.IdComentario);
                DenunciaComentario dc = new DenunciaComentario();
                foreach (var grupo in grupos)
                {
                    if (grupo.Count() > 3)
                    {
                        dc.IdComentario = grupo.FirstOrDefault().IdComentario;
                        dc.FechaHoraDenuncia = grupo.FirstOrDefault().FechaHoraDenuncia;
                        List<DenunciaComentarioDetalle> detalle = new List<DenunciaComentarioDetalle>();
                        foreach (var valor in grupo)
                        {
                            DenunciaComentarioDetalle val = new DenunciaComentarioDetalle();
                            val.IdUsuario = valor.IdUsuario;
                            val.MotivoDenuncia = valor.MotivoDenuncia;
                            detalle.Add(val);
                        }
                        dc.Detalle = detalle;
                    }
                }

                return dc;
            }
        }

        public static List<DenunciasComentarios> ObtenerListaDenunciasPorComentario(int idComentario)
        {
            using (Entities context = new Entities())
            {
                List<DenunciasComentarios> listaDenuncias = context.DenunciasComentarios.Where(x => x.IdComentario == idComentario).ToList();
                return (listaDenuncias);
            }

        }

        public static ComentariosTemas ObtenerComentarioPorIdComentario(int idComentario)
        {
            using (Entities context = new Entities())
            {
                return context.ComentariosTemas.Where(x => x.IdComentario == idComentario).SingleOrDefault();
            }
        }

        public static Usuarios ObtenerUsuarioDenunciado(int IdComentario)
        {
            using (Entities context = new Entities())
            {
                ComentariosTemas comentario = context.ComentariosTemas.Where(x => x.IdComentario == IdComentario).FirstOrDefault();
                Usuarios usuario = context.Usuarios.Where(x => x.IdUsuario == comentario.IdUsuario).SingleOrDefault();

                return usuario;
            }
        }

        public static void BloquearComentario(int IdComentario)
        {
            using (Entities context = new Entities())
            {
                ComentariosTemas comen = context.ComentariosTemas.Where(x => x.IdComentario == IdComentario).FirstOrDefault();
                comen.ComentarioBloqueado = true;

                List<DenunciasComentarios> denuncias = context.DenunciasComentarios.Where(x => x.IdComentario == IdComentario).ToList();
                denuncias.ForEach(x => x.DenunciaRevisada = true);

                context.SaveChanges();
            }
        }

        public static void RechazarDenuncia(int IdComentario)
        {
            using (Entities context = new Entities())
            {
                List<DenunciasComentarios> denuncias = context.DenunciasComentarios.Where(x => x.IdComentario == IdComentario).ToList();
                denuncias.ForEach(x => x.DenunciaRevisada = true);

                context.SaveChanges();

            }
        }

    }
}
