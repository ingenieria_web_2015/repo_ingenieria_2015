﻿using Datos;
using RepositorioClases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicios
{
    public static class UsuarioServicio
    {
        //Crear un nuevo usuario
        public static void NuevoUsuario(Usuario usuario)
        {
            using (Entities context = new Entities())
            {
                context.Usuarios.Add(new Usuarios()
                {
                    Contraseña = usuario.Contraseña,
                    Direccion = usuario.Direccion,
                    FechaNacimiento = usuario.FechaNacimiento,
                    IdEstado = usuario.IdEstado,
                    IdCiudad = usuario.IdCiudad,
                    IdRol = usuario.IdRol,
                    IdUsuario = usuario.IdUsuario,
                    Mail = usuario.Mail,
                    NombreUsuario = usuario.NombreUsuario,
                    NombreApellido = usuario.NombreApellido,
                    FotoPerfil = usuario.FotoPerfil,
                    IdFacebook = usuario.IdFacebook,
                    IdTwitter = usuario.IdTwitter
                });

                context.SaveChanges();
            }
        }

        public static List<Usuario> ObtenerUsuario(int? id)
        {
            using (Entities context = new Entities())
            {
                // la propiedad HasValue de los objetos que son Nullables o permiten tomar valores nulos, equivale a hacer la pregunta if (valor != null)
                // la expresión id.HasValue ? id.Value == u.Id : true
                // equivale a (id.HasValue && id.Value == u.Id) || (!id.HasValue)
                // lo que hacen es filtrar solo cuando viene un valor en la variable, ahorrándonos hacer un if antes y repetir la consulta.
                return context.Usuarios.Where(u => (id.HasValue ? id.Value == u.IdUsuario : true) && u.IdEstado == 1).Select(u => new Usuario()
                {
                    Contraseña = u.Contraseña,
                    Direccion = u.Direccion,
                    FechaNacimiento = u.FechaNacimiento,
                    IdEstado = u.IdEstado,
                    IdCiudad = u.IdCiudad,
                    IdRol = u.IdRol,
                    IdUsuario = u.IdUsuario,
                    Mail = u.Mail,
                    NombreUsuario = u.NombreUsuario,
                    NombreApellido = u.NombreApellido,
                    FotoPerfil = u.FotoPerfil,
                    IdFacebook = u.IdFacebook,
                    IdTwitter = u.IdTwitter
                }).ToList();
            }
        }

        //Editar un nuevo usuario
        public static void EditarUsuario(Usuario usuario)
        {
            using (Entities context = new Entities())
            {
                Usuarios users = context.Usuarios.Where(u => u.IdUsuario == usuario.IdUsuario).FirstOrDefault();
                if (users != null)
                {
                    users.Contraseña = usuario.Contraseña;
                    users.Direccion = usuario.Direccion;
                    users.FechaNacimiento = usuario.FechaNacimiento;
                    users.IdEstado = usuario.IdEstado;
                    users.IdCiudad = usuario.IdCiudad;
                    users.IdRol = usuario.IdRol;
                    users.IdUsuario = usuario.IdUsuario;
                    users.Mail = usuario.Mail;
                    users.NombreUsuario = usuario.NombreUsuario;
                    users.NombreApellido = usuario.NombreApellido;
                    users.FotoPerfil = usuario.FotoPerfil;
                    users.IdTwitter = usuario.IdTwitter;
                    users.IdFacebook = usuario.IdTwitter;
                }

                context.SaveChanges();
            }
        }

        //Eliminar un usuario
        public static void EliminarUsuario(Usuario usuario)
        {
            using (Entities context = new Entities())
            {
                Usuarios user = context.Usuarios.Where(u => u.IdUsuario == usuario.IdUsuario).FirstOrDefault();

                // FirstOrDefault va a intentar recuperar el registro que cumpla la condición
                // si no encuentra ninguno, devuelve NULL, de ahí el siguiente IF.
                if (user != null)
                    user.IdEstado = 4;

                // el objeto en memoria persiste los cambios en la base de datos cuando hago un save sobre el contexto.
                context.SaveChanges();
            }
        }

        public static List<Rol> ObtenerRol(int? id)
        {
            using (Entities context = new Entities())
            {
                // la propiedad HasValue de los objetos que son Nullables o permiten tomar valores nulos, equivale a hacer la pregunta if (valor != null)
                // la expresión id.HasValue ? id.Value == u.Id : true
                // equivale a (id.HasValue && id.Value == u.Id) || (!id.HasValue)
                // lo que hacen es filtrar solo cuando viene un valor en la variable, ahorrándonos hacer un if antes y repetir la consulta.
                return context.Roles.Where(r => id.HasValue ? id.Value == r.IdRol : true).Select(r => new Rol()
                {
                    DescripcionRol = r.DescripcionRol,
                    IdRol = r.IdRol
                }).ToList();
            }
        }

        public static List<Rol> ObtenerBloqueo(int? id)
        {
            using (Entities context = new Entities())
            {
                // la propiedad HasValue de los objetos que son Nullables o permiten tomar valores nulos, equivale a hacer la pregunta if (valor != null)
                // la expresión id.HasValue ? id.Value == u.Id : true
                // equivale a (id.HasValue && id.Value == u.Id) || (!id.HasValue)
                // lo que hacen es filtrar solo cuando viene un valor en la variable, ahorrándonos hacer un if antes y repetir la consulta.
                return context.Estados.Where(b => id.HasValue ? id.Value == b.IdEstado : true).Select(b => new Rol()
                {
                    DescripcionRol = b.DescripcionBloqueo,
                    IdRol = b.IdEstado
                }).ToList();
            }
        }

        public static Usuario ObtenerUsuarioPorNombreContraseña(string mail, string contraseña, int idEstado)
        {
            contraseña = Encriptado.Encriptar(contraseña);
            Usuario user = new Usuario();
            using (Entities context = new Entities())
            {
                Usuarios usu = context.Usuarios.Where(x => x.Mail == mail && x.Contraseña == contraseña && x.IdEstado == idEstado).FirstOrDefault();
                if (usu != null)
                {
                    user.Contraseña = usu.Contraseña;
                    user.Direccion = usu.Direccion;
                    user.FechaNacimiento = usu.FechaNacimiento;
                    user.IdEstado = usu.IdEstado;
                    user.IdCiudad = usu.IdCiudad;
                    user.IdRol = usu.IdRol;
                    user.IdUsuario = usu.IdUsuario;
                    user.Mail = usu.Mail;
                    user.NombreUsuario = usu.NombreUsuario;
                    user.NombreApellido = usu.NombreApellido;
                    user.FotoPerfil = usu.FotoPerfil;
                    user.IdTwitter = usu.IdTwitter;
                    user.IdFacebook = usu.IdFacebook;
                    return user;
                }
                else
                {
                    return null;
                }
            }
        }

        public static bool ConfirmarMail(int id)
        {
            using (Entities context = new Entities())
            {
                Usuarios usu = context.Usuarios.Where(x => x.IdUsuario == id).SingleOrDefault();
                if (usu != null)
                {
                    if (usu.IdEstado == 1)
                    {
                        return false;
                    }
                    else {
                        usu.IdEstado = 1;
                        context.SaveChanges();
                        return true;    
                    }
                }
                else
                {
                    return false;
                }
            }
        }

        public static bool ControlarUsuarioRepetido(string email)
        {
            using (Entities context = new Entities())
            {
                Usuarios usu = context.Usuarios.Where(x => x.Mail == email && x.IdEstado == 1).SingleOrDefault();
                if (usu == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        public static Usuario ObtenerUsuarioPorNombre(string usuario)
        {
            Usuario user = new Usuario();
            using (Entities context = new Entities())
            {
                Usuarios usu = context.Usuarios.Where(x => x.NombreUsuario == usuario && x.IdEstado == 1).FirstOrDefault();
                if (usu != null)
                {
                    user.Contraseña = usu.Contraseña;
                    user.Direccion = usu.Direccion;
                    user.FechaNacimiento = usu.FechaNacimiento;
                    user.IdEstado = usu.IdEstado;
                    user.IdCiudad = usu.IdCiudad;
                    user.IdRol = usu.IdRol;
                    user.IdUsuario = usu.IdUsuario;
                    user.Mail = usu.Mail;
                    user.NombreUsuario = usu.NombreUsuario;
                    user.NombreApellido = usu.NombreApellido;
                    user.FotoPerfil = usu.FotoPerfil;
                    user.IdFacebook = usu.IdFacebook;
                    user.IdTwitter = usu.IdTwitter;
                    return user;
                }
                else
                {
                    return null;
                }
            }
        }

        public static List<Ciudades> ObtenerCiudades()
        {
            using (Entities context = new Entities())
            {
                List<Ciudades> ciudades = context.Ciudades.ToList();
                return ciudades;
            }

        }

        public static Ciudad ObtenerCiudadPorIdCiudad(int? IdCiudad)
        {
            using (Entities context = new Entities())
            {
                Ciudad ciu = context.Ciudades.Where(t => t.CiudadId == IdCiudad).Select(x => new Ciudad()
                {
                    CiudadId = x.CiudadId,
                    CiudadNombre = x.CiudadNombre
                }).SingleOrDefault();
                return ciu;
            }
        }

        public static bool ConsultarIdFacebook(string idFacebook)
        {
            using (Entities context = new Entities())
            {
                UsuariosFacebook usuFacebook = context.UsuariosFacebook.Where(u => u.IdFacebook == idFacebook).SingleOrDefault();
                if (usuFacebook == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        public static bool ConsultarIdTwitter(string idTwitter)
        {
            using (Entities context = new Entities())
            {
                UsuariosTwitter usuTwitter = context.UsuariosTwitter.Where(u => u.IdTwitter == idTwitter).SingleOrDefault();
                if (usuTwitter == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        public static bool CambiarContrasena(string contra, int id)
        {
            using (Entities context = new Entities())
            {
                Usuarios usu = context.Usuarios.Where(u => u.IdUsuario == id).SingleOrDefault();
                usu.Contraseña = Encriptado.Encriptar(contra);
                context.SaveChanges();
                return true;
            }
        }

        public static UsuarioFacebook ObtenerUsuarioFacebookPorId(string idFacebook)
        {
            using (Entities context = new Entities())
            {
                UsuarioFacebook usuFacebook = context.UsuariosFacebook.Where(u => u.IdFacebook == idFacebook).Select(x => new UsuarioFacebook() { 
                    IdFacebook = x.IdFacebook,
                    Link = x.LinkFacebook,
                    Mail = x.Mail,
                    Name = x.Nombre,
                    AccessToken = x.AccessToken,
                    Verificado = x.Verificado
                }).SingleOrDefault();
                return usuFacebook;
            }
        }

        public static UsuarioTwitter ObtenerUsuarioTwitterPorId(string idTwitter)
        {
            using (Entities context = new Entities())
            {
                UsuarioTwitter usuTwitter = context.UsuariosTwitter.Where(u => u.IdTwitter == idTwitter).Select(x => new UsuarioTwitter()
                {
                    IdTwitter = x.IdTwitter,
                    UserName = x.UserName,
                    AccessToken = x.AccessToken,
                    Mail = x.Mail
                }).SingleOrDefault();
                return usuTwitter;
            }
        }

        public static void GuardarUsuarioFacebook(UsuarioFacebook usuario)
        {
            using (Entities context = new Entities())
            {
                UsuariosFacebook usuFacebook = new UsuariosFacebook()
                {
                    IdFacebook = usuario.IdFacebook,
                    Nombre = usuario.Name,
                    LinkFacebook = usuario.Link,
                    Mail = usuario.Mail,
                    AccessToken = usuario.AccessToken,
                    Verificado = false
                };
                context.UsuariosFacebook.Add(usuFacebook);
                context.SaveChanges();
            }
        }

        public static void GuardarUsuarioTwitter(UsuarioTwitter usuario)
        {
            using (Entities context = new Entities())
            {
                UsuariosTwitter usuTwitter = new UsuariosTwitter()
                {
                    IdTwitter = usuario.IdTwitter,
                    UserName = usuario.UserName,
                    AccessToken = usuario.AccessToken,
                    Mail = usuario.Mail
                };
                context.UsuariosTwitter.Add(usuTwitter);
                context.SaveChanges();
            }
        }

        public static string GenerarClaveAleatoria()
        { 
            //Generar una clave aleatoria y devolverla encriptada
            return "clave";
        }

        public static void RelacionarCuentaFacebook(UsuarioFacebook usuFacebook) 
        {
            using (Entities context = new Entities())
            {
                Usuarios u = context.Usuarios.Where(x => x.Mail == usuFacebook.Mail).SingleOrDefault();
                u.IdFacebook = usuFacebook.IdFacebook;
                context.SaveChanges();
            } 
        }

        public static void RelacionarCuentaTwitter(UsuarioTwitter usuTwitter)
        {
            using (Entities context = new Entities())
            {
                Usuarios u = context.Usuarios.Where(x => x.Mail == usuTwitter.Mail).SingleOrDefault();
                u.IdTwitter = usuTwitter.IdTwitter;
                context.SaveChanges();
            }
        }

        public static Usuario ObtenerUsuarioPorIdFacebook(string IdFacebook)
        {
            Usuario user = new Usuario();
            using (Entities context = new Entities())
            {
                Usuarios usu = context.Usuarios.Where(x => x.IdFacebook == IdFacebook).FirstOrDefault();
                if (usu != null)
                {
                    user.Contraseña = usu.Contraseña;
                    user.Direccion = usu.Direccion;
                    user.FechaNacimiento = usu.FechaNacimiento;
                    user.IdEstado = usu.IdEstado;
                    user.IdCiudad = usu.IdCiudad;
                    user.IdRol = usu.IdRol;
                    user.IdUsuario = usu.IdUsuario;
                    user.Mail = usu.Mail;
                    user.NombreUsuario = usu.NombreUsuario;
                    user.NombreApellido = usu.NombreApellido;
                    user.FotoPerfil = usu.FotoPerfil;
                    user.IdFacebook = usu.IdFacebook;
                    user.IdTwitter = usu.IdTwitter;
                    return user;
                }
                else
                {
                    return null;
                }
            }
        }

        public static Usuario ObtenerUsuarioPorEmail(string mail)
        {
            Usuario user = new Usuario();
            using (Entities context = new Entities())
            {
                Usuarios usu = context.Usuarios.Where(x => x.Mail == mail).FirstOrDefault();
                if (usu != null)
                {
                    user.Contraseña = usu.Contraseña;
                    user.Direccion = usu.Direccion;
                    user.FechaNacimiento = usu.FechaNacimiento;
                    user.IdEstado = usu.IdEstado;
                    user.IdCiudad = usu.IdCiudad;
                    user.IdRol = usu.IdRol;
                    user.IdUsuario = usu.IdUsuario;
                    user.Mail = usu.Mail;
                    user.NombreUsuario = usu.NombreUsuario;
                    user.NombreApellido = usu.NombreApellido;
                    user.FotoPerfil = usu.FotoPerfil;
                    user.IdFacebook = usu.IdFacebook;
                    user.IdTwitter = usu.IdTwitter;
                    return user;
                }
                else
                {
                    return null;
                }
            }
        }

        public static Usuario ObtenerUsuarioPorIdTwitter(string idTwitter)
        {
            Usuario user = new Usuario();
            using (Entities context = new Entities())
            {
                Usuarios usu = context.Usuarios.Where(x => x.IdTwitter == idTwitter).FirstOrDefault();
                if (usu != null)
                {
                    user.Contraseña = usu.Contraseña;
                    user.Direccion = usu.Direccion;
                    user.FechaNacimiento = usu.FechaNacimiento;
                    user.IdEstado = usu.IdEstado;
                    user.IdCiudad = usu.IdCiudad;
                    user.IdRol = usu.IdRol;
                    user.IdUsuario = usu.IdUsuario;
                    user.Mail = usu.Mail;
                    user.NombreUsuario = usu.NombreUsuario;
                    user.NombreApellido = usu.NombreApellido;
                    user.FotoPerfil = usu.FotoPerfil;
                    user.IdFacebook = usu.IdFacebook;
                    user.IdTwitter = usu.IdTwitter;
                    return user;
                }
                else
                {
                    return null;
                }
            }
        }
    }
}
