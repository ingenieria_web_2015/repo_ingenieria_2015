﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;
using RepositorioClases;

namespace Servicios
{
    public class TemaService
    {
        public static void NuevoTema(Tema tema)
        {
            using (Entities context = new Entities())
            {
                context.Temas.Add(new Temas()
                {
                    IdUsuario = tema.IdUsuario,
                    FechaHoraTema = tema.FechaHoraTema,
                    TituloTema = tema.TituloTema,
                    DescripcionTema = tema.DescripcionTema,
                    IdCiudad = tema.IdCiudad,
                    ContenidoTema = tema.ContenidoTema,
                    IdCategoria = tema.IdCategoria,
                    UrlYoutube = tema.UrlYoutube,
                    IdGaleria = tema.IdGaleria
                });

                context.SaveChanges();
            }
        }

        public static List<Tema> ObtenerTema(int? id)
        {
            using (Entities context = new Entities())
            {
                return context.Temas.Where(u => (id.HasValue ? id.Value == u.IdUsuario : true) && u.FechaEliminacion == null).Select(u => new Tema()
                {
                    IdUsuario = u.IdUsuario,
                    IdTema = u.IdTema,
                    FechaHoraTema = u.FechaHoraTema,
                    TituloTema = u.TituloTema,
                    DescripcionTema = u.DescripcionTema,
                    IdCiudad = u.IdCiudad,
                    ContenidoTema = u.ContenidoTema,
                    FechaEliminacion = u.FechaEliminacion,
                    IdCategoria = u.IdCategoria,
                    UrlYoutube = u.UrlYoutube,
                    IdGaleria = u.IdGaleria
                }).OrderByDescending(x => x.FechaHoraTema).ToList();

            }
        }

        public static List<Tema> ObtenerTemaPorIdTema(int? id)
        {
            using (Entities context = new Entities())
            {
                return context.Temas.Where(u => (id.HasValue ? id.Value == u.IdTema : true) && u.FechaEliminacion == null).Select(u => new Tema()
                {
                    IdUsuario = u.IdUsuario,
                    IdTema = u.IdTema,
                    FechaHoraTema = u.FechaHoraTema,
                    TituloTema = u.TituloTema,
                    DescripcionTema = u.DescripcionTema,
                    IdCiudad = u.IdCiudad,
                    ContenidoTema = u.ContenidoTema,
                    FechaEliminacion = u.FechaEliminacion,
                    IdCategoria = u.IdCategoria,
                    UrlYoutube = u.UrlYoutube,
                    IdGaleria = u.IdGaleria
                }).ToList();
            }
        }

        public static List<Tema> ObtenerTemasPorIdCategoria(int? id)
        {
            using (Entities context = new Entities())
            {
                return context.Temas.Where(u => (id.HasValue ? id.Value == u.IdCategoria : true) && u.FechaEliminacion == null).Select(u => new Tema()
                {
                    IdUsuario = u.IdUsuario,
                    IdTema = u.IdTema,
                    FechaHoraTema = u.FechaHoraTema,
                    TituloTema = u.TituloTema,
                    DescripcionTema = u.DescripcionTema,
                    IdCiudad = u.IdCiudad,
                    ContenidoTema = u.ContenidoTema,
                    FechaEliminacion = u.FechaEliminacion,
                    IdCategoria = u.IdCategoria,
                    UrlYoutube = u.UrlYoutube,
                    IdGaleria = u.IdGaleria
                }).ToList();
            }
        }

        public static void EditarTema(Tema tema)
        {
            using (Entities context = new Entities())
            {
                Temas tem = context.Temas.Where(g => g.IdTema == tema.IdTema).FirstOrDefault();

                if (tem != null)
                {
                    tem.FechaHoraTema = tema.FechaHoraTema;
                    tem.TituloTema = tema.TituloTema;
                    tem.DescripcionTema = tema.DescripcionTema;
                    tem.IdCiudad = tema.IdCiudad;
                    tem.IdTema = tema.IdTema;
                    tem.ContenidoTema = tema.ContenidoTema;
                    tem.FechaEliminacion = tema.FechaEliminacion;
                    tem.IdCategoria = tema.IdCategoria;
                    tem.UrlYoutube = tema.UrlYoutube;
                    tem.IdGaleria = tema.IdGaleria;
                }
                context.SaveChanges();
            }
        }

        public static void EliminarTema(int idTema)
        {
            using (Entities context = new Entities())
            {
                Temas tem = context.Temas.Where(t => t.IdTema == idTema).FirstOrDefault();
                tem.FechaEliminacion = DateTime.Now;

                context.SaveChanges();
            }
        }


        public static Tema ObtenerTemaPorIdUsuarioFecha(int IdUsuario, DateTime Fecha)
        {
            using (Entities context = new Entities())
            {
                Tema tem = context.Temas.Where(t => t.IdUsuario == IdUsuario && t.FechaEliminacion == null && t.FechaHoraTema == Fecha).Select(x => new Tema()
                {
                    IdUsuario = x.IdUsuario,
                    IdTema = x.IdTema,
                    FechaHoraTema = x.FechaHoraTema,
                    TituloTema = x.TituloTema,
                    DescripcionTema = x.DescripcionTema,
                    IdCiudad = x.IdCiudad,
                    FechaEliminacion = x.FechaEliminacion,
                    ContenidoTema = x.ContenidoTema,
                    IdCategoria = x.IdCategoria,
                    UrlYoutube = x.UrlYoutube,
                    IdGaleria = x.IdGaleria
                }).SingleOrDefault();
                return tem;
            }
        }

        public static List<CategoriasTemas> ObtenerCategorias(int? id)
        {
            using (Entities context = new Entities())
            {
                return context.CategoriasTemas.Where(c => (id.HasValue ? id.Value == c.IdCategoria : true)).ToList();
            }
        }

        public static List<Ciudades> ObtenerCiudades()
        {
            using (Entities context = new Entities())
            {
                List<Ciudades> ciudades = context.Ciudades.ToList();
                return ciudades;
            }

        }

        public static Ciudad ObtenerCiudadPorIdCiudad(int? IdCiudad)
        {
            using (Entities context = new Entities())
            {
                Ciudad ciu = context.Ciudades.Where(t => t.CiudadId == IdCiudad).Select(x => new Ciudad()
                {
                    CiudadId = x.CiudadId,
                    CiudadNombre = x.CiudadNombre
                }).SingleOrDefault();
                return ciu;
            }
        }

        public static bool GuardarComentario(string comentario, int idTema, int idUsuario)
        {
            using (Entities context = new Entities())
            {
                try
                {
                    context.ComentariosTemas.Add(new ComentariosTemas()
                    {
                        IdTema = idTema,
                        IdUsuario= idUsuario,
                        FechaHoraComentario = DateTime.Now,
                        DescripcionComentario = comentario,
                    });

                    context.SaveChanges();
                    return true;
                }
                catch (Exception e) {
                    return false;
                }
            }
        }

        public static List<Comentario> ObtenerComentariosPorTema(int idTema)
        {
            using (Entities context = new Entities())
            {
                return context.ComentariosTemas.Where(x => x.IdTema == idTema && x.FechaHoraEliminacion == null && x.ComentarioBloqueado==null ).Select(x => new Comentario()
                {
                    IdUsuario=x.IdUsuario,
                    DescripcionComentario = x.DescripcionComentario,
                    FechaHoraComentario = x.FechaHoraComentario,
                    IdTema = x.IdTema,
                    IdComentario = x.IdComentario,
                    ComentarioBloqueado=x.ComentarioBloqueado,

                }).ToList();
            }
        }

        public static Comentario ObtenerComentario(int idComentario)
        {
            using (Entities context = new Entities())
            {
                return context.ComentariosTemas.Where(x => x.IdComentario == idComentario && x.FechaHoraEliminacion == null && x.ComentarioBloqueado == null).Select(x => new Comentario()
                {
                    IdUsuario = x.IdUsuario,
                    DescripcionComentario = x.DescripcionComentario,
                    FechaHoraComentario = x.FechaHoraComentario,
                    IdTema = x.IdTema,
                    IdComentario = x.IdComentario,
                    ComentarioBloqueado = x.ComentarioBloqueado,

                }).SingleOrDefault();
            }
        }

        public static bool EditarComentario(Comentario comentario)
        {
            using (Entities context = new Entities())
            {
                try
                {
                    ComentariosTemas coment = context.ComentariosTemas.Where(g => g.IdComentario == comentario.IdComentario).SingleOrDefault();
                    coment.IdComentario = comentario.IdComentario;
                    coment.DescripcionComentario = comentario.DescripcionComentario;
                    coment.FechaHoraComentario = comentario.FechaHoraComentario;
                    coment.FechaHoraEliminacion = comentario.FechaHoraEliminacion;
                    coment.ComentarioBloqueado = comentario.ComentarioBloqueado;
                   

                    context.SaveChanges();
                    return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
 
        }

        public static bool EliminarComentario(int IdComentario)
        {
            using (Entities context = new Entities())
            {                
                try
                {
                ComentariosTemas coment = context.ComentariosTemas.Where(g => g.IdComentario == IdComentario).FirstOrDefault();
                coment.FechaHoraEliminacion = DateTime.Now;

                context.SaveChanges();
                return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
        }

    }
}
