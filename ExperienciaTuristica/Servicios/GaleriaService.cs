﻿using Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RepositorioClases;

namespace Servicios
{
    public class GaleriaService
    {
        public static void NuevaGaleria(Galeria galeria)
        {
            using (Entities context = new Entities())
            {
                context.Galerias.Add(new Galerias()
                {
                    IdUsuario = galeria.IdUsuario,
                    FechaHoraGaleria = galeria.FechaHoraGaleria,
                    TituloGaleria = galeria.TituloGaleria,
                    DescripcionGaleria = galeria.DescripcionGaleria,
                    CiudadId = galeria.CiudadId
                });

                context.SaveChanges();
            }
        }

        public static List<Galeria> ObtenerGaleria(int? id)
        {
            using (Entities context = new Entities())
            {
                return context.Galerias.Where(u => (id.HasValue ? id.Value == u.IdUsuario : true) && u.FechaEliminacion == null).Select(u => new Galeria()
                {
                    IdUsuario = u.IdUsuario,
                    IdGaleria = u.IdGaleria,
                    FechaHoraGaleria = u.FechaHoraGaleria,
                    TituloGaleria = u.TituloGaleria,
                    DescripcionGaleria = u.DescripcionGaleria,
                    CiudadId = u.CiudadId,
                    FechaEliminacion = u.FechaEliminacion
                }).OrderByDescending(x => x.FechaHoraGaleria).ToList();
            }
        }

        public static List<Galeria> ObtenerGaleriaPorIdGaleria(int? id)
        {
            using (Entities context = new Entities())
            {
                return context.Galerias.Where(u => (id.HasValue ? id.Value == u.IdGaleria : true) && u.FechaEliminacion == null).Select(u => new Galeria()
                {
                    IdUsuario = u.IdUsuario,
                    IdGaleria = u.IdGaleria,
                    FechaHoraGaleria = u.FechaHoraGaleria,
                    TituloGaleria = u.TituloGaleria,
                    DescripcionGaleria = u.DescripcionGaleria,
                    CiudadId = u.CiudadId,
                    FechaEliminacion = u.FechaEliminacion
                }).ToList();
            }
        }

        //Editar un nuevo usuario
        public static void EditarGaleria(Galeria galeria)
        {
            using (Entities context = new Entities())
            {
                Galerias gal = context.Galerias.Where(g => g.IdGaleria == galeria.IdGaleria).FirstOrDefault();

                if (gal != null)
                {
                    gal.FechaHoraGaleria = galeria.FechaHoraGaleria;
                    gal.TituloGaleria = galeria.TituloGaleria;
                    gal.DescripcionGaleria = galeria.DescripcionGaleria;
                    gal.CiudadId = galeria.CiudadId;
                    gal.IdGaleria = galeria.IdGaleria;
                    gal.FechaEliminacion = galeria.FechaEliminacion;
                }

                context.SaveChanges();
            }
        }

        //Eliminar un usuario
        public static void EliminarGaleria(int idGaleria)
        {
            using (Entities context = new Entities())
            {
                Galerias gal = context.Galerias.Where(g => g.IdGaleria == idGaleria).FirstOrDefault();
                gal.FechaEliminacion = DateTime.Now;

                context.SaveChanges();
            }
        }

        public static Galeria ObtenerGaleriaPorIdUsuarioFecha(int IdUsuario, DateTime Fecha)
        {
            using (Entities context = new Entities())
            {
                Galeria gal = context.Galerias.Where(g => g.IdUsuario == IdUsuario && g.FechaEliminacion == null && g.FechaHoraGaleria == Fecha).Select(x => new Galeria()
                {
                    IdUsuario = x.IdUsuario,
                    IdGaleria = x.IdGaleria,
                    FechaHoraGaleria = x.FechaHoraGaleria,
                    TituloGaleria = x.TituloGaleria,
                    DescripcionGaleria = x.DescripcionGaleria,
                    CiudadId = x.CiudadId,
                    FechaEliminacion = x.FechaEliminacion
                }).SingleOrDefault();
                return gal;
            }
        }

        public static void GuardarImagen(string path, int idGaleria)
        {
            using (Entities context = new Entities())
            {
                context.ImagenesGalerias.Add(new ImagenesGalerias()
                {
                    IdGaleria = idGaleria,
                    FechaHoraImagen = DateTime.Now,
                    PathImagen = path,
                });

                context.SaveChanges();
            }
        }

        public static List<Imagen> ObtenerImagenesPorGaleria(int idGaleria)
        {
            using (Entities context = new Entities())
            {
                return context.ImagenesGalerias.Where(x => x.IdGaleria == idGaleria && x.FechaEliminacion == null).Select(x => new Imagen()
                {
                    DescripcionImagen = x.DescripcionImagen,
                    FechaHoraImagen = x.FechaHoraImagen,
                    IdGaleria = x.IdGaleria,
                    IdImagen = x.IdImagen,
                    PathImagen = x.PathImagen
                }).ToList();
            }
        }

        public static bool ActualizarComentarioImagen(int idImagen, string descripcion)
        {
            using (Entities context = new Entities())
            {
                ImagenesGalerias gal = context.ImagenesGalerias.Where(x => x.IdImagen == idImagen).SingleOrDefault();
                gal.DescripcionImagen = descripcion;
                context.SaveChanges();
                return true;
            }
        }

        public static void EliminarImagen(int IdImagen)
        {
            using (Entities context = new Entities())
            {
                ImagenesGalerias img = context.ImagenesGalerias.Where(g => g.IdImagen == IdImagen).FirstOrDefault();
                img.FechaEliminacion = DateTime.Now;

                context.SaveChanges();
            }
        }
    }
}
