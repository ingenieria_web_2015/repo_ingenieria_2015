﻿using Datos;
using RepositorioClases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicios
{
    public class ComentarioService
    {
        public static Usuario ObtenerUsuarioPorIdUsuario(int? IdUsuario)
        {
            using (Entities context = new Entities())
            {
                Usuario usu = context.Usuarios.Where(t => t.IdUsuario == IdUsuario).Select(x => new Usuario()
                {
                    IdUsuario=x.IdUsuario,
                    NombreUsuario = x.NombreUsuario,
                    NombreApellido=x.NombreApellido,
                    FotoPerfil=x.FotoPerfil
                }).SingleOrDefault();
                return usu;
            }
        }

        public static bool GuardarDenuncia(int idComentario, int idUsuario, string motivoDenuncia )
        {
            using (Entities context = new Entities())
            {
                try
                {
                    context.DenunciasComentarios.Add(new DenunciasComentarios()
                    {
                        IdComentario = idComentario,
                        IdUsuario = idUsuario,
                        FechaHoraDenuncia = DateTime.Now,
                        MotivoDenuncia = motivoDenuncia,
                    });

                    context.SaveChanges();
                    return true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
        }
    }
}
