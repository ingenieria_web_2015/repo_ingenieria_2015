﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RepositorioClases;
using Datos;

namespace Servicios
{
    public static class PaisServicio
    {
        public static List<Paises> ObtenerPaises()
        {
            using (Entities context = new Entities())
            {
                var p = context.Paises.OrderBy(x => x.PaisNombre);
                List<Paises> paises = p.ToList();
                return paises;
            }

        }

        public static Paises ObtenerPaisPorIdCiudad(int? idCiudad)
        {
            using (Entities context = new Entities())
            {
                if (idCiudad == 0)
                {
                    return null;
                }
                else
                {
                    Ciudades Ciudad = context.Ciudades.Where(x => x.CiudadId == idCiudad).SingleOrDefault();
                    Paises Pais = context.Paises.Where(x => x.PaisCodigo == Ciudad.PaisCodigo).SingleOrDefault();
                    return Pais;
                }
            }

        }
    }


}
