﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;
using RepositorioClases;

namespace Servicios
{
    public static class EstadoServicio
    {
        public static List<Estados> ObtenerEstados()
        {
            using (Entities context = new Entities())
            {
                List<Estados> estados = context.Estados.ToList();
                return estados;
            }

        }

        public static Estado ObtenerEstadoPorIdEstado(int? IdEstado)
        {
            using (Entities context = new Entities())
            {
                Estado est = context.Estados.Where(t => t.IdEstado == IdEstado).Select(x => new Estado()
                {
                    IdEstado = x.IdEstado,
                    DescripcionBloqueo = x.DescripcionBloqueo
                }).SingleOrDefault();
                return est;
            }
        }
    }
}
