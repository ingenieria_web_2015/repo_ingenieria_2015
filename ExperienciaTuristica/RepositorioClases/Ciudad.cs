﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositorioClases
{
    public class Ciudad
    {
        public int CiudadId { get; set; }

        public string CiudadNombre { get; set; }

        public string PaisCodigo { get; set; }
        //public Pais PaisNombre { get; set; }

    }
}
