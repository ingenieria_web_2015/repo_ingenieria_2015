﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositorioClases
{
    public class Rol
    {
        public int IdRol { get; set; }

        public string DescripcionRol { get; set; }
    }
}
