﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositorioClases
{
    public class Estado
    {
        public int IdEstado { get; set; }
        public string DescripcionBloqueo { get; set; }
    }
}
