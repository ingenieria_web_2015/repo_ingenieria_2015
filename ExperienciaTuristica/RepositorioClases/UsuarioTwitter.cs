﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositorioClases
{
    public class UsuarioTwitter
    {
        public string IdTwitter { get; set; }
        public string AccessToken { get; set; }
        public string UserName { get; set; }
        public string Mail { get; set; }
    }
}
