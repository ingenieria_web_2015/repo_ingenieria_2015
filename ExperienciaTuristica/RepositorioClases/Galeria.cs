﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositorioClases
{
    public class Galeria
    {
        public int IdGaleria { get; set; }
        public int IdUsuario { get; set; }
        public System.DateTime FechaHoraGaleria { get; set; }
        public string TituloGaleria { get; set; }
        public string DescripcionGaleria { get; set; }
        public int CiudadId { get; set; }
        public DateTime? FechaEliminacion { get; set; }
    }
}
