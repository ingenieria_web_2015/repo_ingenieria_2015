﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositorioClases
{
    public class DenunciaComentario
    {    
        public int IdComentario { get; set; }
        public DateTime? FechaHoraDenuncia { get; set; }
        public List<DenunciaComentarioDetalle> Detalle { get; set; }
    }

    public class DenunciaComentarioDetalle {
        public int IdUsuario { get; set; }
        public string MotivoDenuncia { get; set; }
    }
}
