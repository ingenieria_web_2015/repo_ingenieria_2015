﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositorioClases
{
    public class ResultadoBusqueda
    {
        public int Id { get; set; }
        public string TituloRepresentativo { get; set; }
        public string TipoResultado { get; set; }
    }
}
