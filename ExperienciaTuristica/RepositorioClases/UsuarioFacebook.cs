﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositorioClases
{
    public class UsuarioFacebook
    {
        public string IdFacebook { get; set; }
        public string Link { get; set; }
        public string Mail { get; set; }
        public string Name { get; set; }
        public string AccessToken { get; set; }
        public bool Verificado { get; set; }
    }
}
