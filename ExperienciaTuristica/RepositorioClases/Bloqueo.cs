﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositorioClases
{
    public class Bloqueo
    {
        public int IdBloqueo { get; set; }
        public string DescripcionBloqueo { get; set; }
    }
}
