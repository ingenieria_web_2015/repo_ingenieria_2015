﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositorioClases
{
    public class Denuncia
    {
        public int IdUsuario { get; set; }

        public int IdComentario { get; set; }

        public string MotivoDenuncia { get; set; }

        public DateTime FechaHoraDenuncia { get; set; }
    }
}
