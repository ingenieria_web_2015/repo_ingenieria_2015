﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositorioClases
{
    public class Comentario
    {
        public int IdTema { get; set; }
        public int IdComentario { get; set; }
        public int IdUsuario { get; set; }
        public DateTime FechaHoraComentario { get; set; }
        public string DescripcionComentario { get; set; }
        public bool? ComentarioBloqueado { get; set; }
        public DateTime? FechaHoraEliminacion { get; set; }
    }
}
