﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositorioClases
{
    public class CategoriaTema
    {
        public int IdCategoria { get; set; }
        public string DescripcionCategoria { get; set; }
        public DateTime? FechaEliminacion { get; set; }
    }
}
