﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositorioClases
{
    public class Usuario
    {
        public int IdUsuario { get; set; }
        public string NombreUsuario { get; set; }
        public string NombreApellido { get; set; }
        public string Mail { get; set; }
        public string Contraseña { get; set; }
        public int IdRol { get; set; }
        public int? IdCiudad { get; set; }
        public string Direccion { get; set; }
        public DateTime? FechaNacimiento { get; set; }
        public int? IdEstado { get; set; }
        public string FotoPerfil { get; set; }
        public string IdFacebook { get; set; }
        public string IdTwitter { get; set; }
    }
}
