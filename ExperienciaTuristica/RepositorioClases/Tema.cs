﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositorioClases
{
    public class Tema
    {
        public int IdTema { get; set; }
        public string TituloTema { get; set; }
        public int IdUsuario { get; set; }
        public System.DateTime FechaHoraTema { get; set; }
        public string DescripcionTema { get; set; }
        public string ContenidoTema { get; set; }
        public int IdCiudad { get; set; }
        public int IdCategoria { get; set; }
        public DateTime? FechaEliminacion { get; set; }
        public string UrlYoutube { get; set; }
        public int? IdGaleria { get; set; }
    }
}
