﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositorioClases
{
    public class Perfil
    {
        public int IdUsuario { get; set; }
        public string NombreUsuario { get; set; }
        public string NombreApellido { get; set; }
        public string Mail { get; set; }
        public int? IdCiudad { get; set; }
        public string FotoPerfil { get; set; }

    }
}
