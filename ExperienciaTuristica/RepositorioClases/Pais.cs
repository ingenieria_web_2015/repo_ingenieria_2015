﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositorioClases
{
    public class Pais
    {
        public string PaisCodigo { get; set; }

        public string PaisNombre { get; set; }

        //public virtual ICollection<Ciudad> Ciudades { get; set; }


    }
}
