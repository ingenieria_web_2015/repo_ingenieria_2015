﻿$(document).ready(function () {
    $("#Busqueda").bind("keydown", function (e) {
        if (e.keyCode == '8') {
            $("#popover-busqueda").empty();
            $("#popover-busqueda").hide();
        }
        else {
            if ($("#Busqueda").val().length > 0) {
                datos = { parametroBusqueda: $("#Busqueda").val() }
                $.ajax({
                    type: "POST",
                    url: "/Busqueda/BuscarTodo/",
                    data: datos,
                    success: function (data) {
                        $("#popover-busqueda").empty();
                        if (data.length == 0) {
                            $("#popover-busqueda").append("No se encontraron resultados");
                        }
                        else {
                            var intTema = 0;
                            var intComentario = 0;
                            var intGaleria = 0;
                            var intCategoria = 0;
                            $.each(data, function (index, value) {
                                if (value.TipoResultado == "Tema") {
                                    if (intTema == 0) {
                                        $("#popover-busqueda").append("<div style='padding-left:3px; font-weight:bold;'>Tema</div>");
                                        intTema = 1;
                                    }
                                    $("#popover-busqueda").append("<div style='padding-left: 20px; padding-right: 5px;'><a style='text-decoration: none' href='/Tema/MostrarTema?IdTema=" + value.Id + "'>" + value.TituloRepresentativo + "</a></div>");
                                }
                                else if (value.TipoResultado == "Comentario") {
                                    if (intComentario == 0) {
                                        $("#popover-busqueda").append("<div style='padding-left:3px; font-weight:bold;'>Comentario</div>");
                                        intComentario = 1;
                                    }
                                    $("#popover-busqueda").append("<div style='padding-left: 20px; padding-right: 5px;'><a style='text-decoration: none' href='/Tema/MostrarTema?IdTema=" + value.Id + "'>" + value.TituloRepresentativo + "</a></div>");
                                }
                                else if (value.TipoResultado == "Categoría") {
                                    if (intCategoria == 0) {
                                        $("#popover-busqueda").append("<div style='padding-left:3px; font-weight:bold;'>Categoría</div>");
                                        intCategoria = 1;
                                    }
                                    $("#popover-busqueda").append("<div style='padding-left: 20px; padding-right: 5px;'><a style='text-decoration: none' href='/Tema/MostrarTemasPorCategoria?IdCategoria=" + value.Id + "'>" + value.TituloRepresentativo + "</a></div>");
                                }
                                else if (value.TipoResultado = "Galeria") {
                                    if (intGaleria == 0) {
                                        $("#popover-busqueda").append("<div style='padding-left:3px; font-weight:bold;'>Galeria</div>");
                                        intTema = 1;
                                    }
                                    $("#popover-busqueda").append("<div style='padding-left: 20px; padding-right: 5px;'><a href='/Galeria/Index?Id=" + value.Id + "'>" + value.TituloRepresentativo + "</a></div>");
                                }
                            });
                        }

                        $("#popover-busqueda").show();


                    }
                });
            }
        }
    });
});

function iniciarSesion() {
    var usu = $('#usuarioSesion').val();
    var cont = $('#contraseñaSesion').val();
    datos = { mail: usu, contraseña: cont }
    $('#ModalInicio .modal-header img').show();
    $.ajax({
        type: "POST",
        url: "/Login/Inicio/",
        data: datos,
        success: function (data) {
            if (data == "IsAdmin") {
                //window.location.href = '/Usuarios/Index/';
                //$('#li-adm').removeClass('no-visible');
                location.reload();

            }
            else {
                if (data == "UsuarioComun") {
                    location.reload();
                }
                else {
                    if (data == "Moderador") {
                        //window.location.href = "/Moderador/Index/";
                        location.reload();
                        //$('#li-mod').removeClass('no-visible');
                    }
                    else {
                        data == "Otro"
                        $('#ErrorUsuario').show()
                    }
                }
                $('#ModalInicio .modal-header img').hide()
            }
        }
    });
}

function busquedaAvanzada() {
    var tit = $('#TituloPublicacion').val();
    var cop = $('#CopetePublicacion').val();
    var desc = $('#DescripcionPublicacion').val();
    var com = $('#ComentarioPublicacion').val();
    datos = { titulo: tit, copete: cop, descripcion: desc, comentario: com }
    $('#ModalBusquedaAvanzada .modal-footer img').show();

    $.get("/Busqueda/BusquedaCompleta/", { titulo: tit, copete: cop, descripcion: desc, comentario: com });
}

function ocultarError() {
    $('#ErrorUsuario').hide()
}

function mostrarCargando() {
    $('#ModalRegistro .modal-footer img').show()
}

function mostrarLoginRedesSociales()
{
    $("#login-comun").hide();
    $("#botones-login-comun").hide();
    $("#login-redes").show();
} 

function mostrarLoginUsuarioComun() {
    $("#login-redes").hide();
    $("#login-comun").show();
    $("#botones-login-comun").show();
}
