﻿function statusChangeCallback(response) {
    console.log('statusChangeCallback');
    console.log(response);
    if (response.status === 'connected') {
        testAPI();
    } else if (response.status === 'not_authorized') {
        document.getElementById('status').innerHTML = 'Please log ' +
          'into this app.';
    } else {
        document.getElementById('status').innerHTML = 'Please log ' +
          'into Facebook.';
    }
}

function checkLoginState() {
    FB.getLoginStatus(function (response) {
        statusChangeCallback(response);
    });
}

window.fbAsyncInit = function () {
    FB.init({
        appId: '1610450629193478',
        cookie: true,
        xfbml: true,
        version: 'v2.2'
    });

    FB.getLoginStatus(function (response) {
        statusChangeCallback(response);
    });

};

(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

function testAPI() {
    console.log('Welcome!  Fetching your information.... ');
    FB.api('/me', function (response) {
        //console.log('Successful login for: ' + response.name);
        //document.getElementById('status').innerHTML =
        //  'Thanks for logging in, ' + response.name + '!';
        var datos = {
            idFacebook: response.id,
            link: response.link,
            mail: response.email,
            name: response.name
        }
        debugger;
        $.ajax({
            type: "POST",
            url: "/Login/InicioFacebook/",
            data: datos,
            success: function (data) {
                //if (data == "Ok")
                //{
                //    alert("Por ser su primer ingreso desde facebook, debe completar su perfil para poder utilizar la comunidad.");
                //    window.location.href = '@Html.GetUrl("Action", "Controller", new { idFacebook='+ datos.idFacebook +'})';
                //    //$.ajax({
                //    //    type: "GET",
                //    //    url: "/Login/CompletarRegistroFacebook/",
                //    //    data: { idFacebook: datos.idFacebook },
                //    //});
                //}
            }
        });
    });
}