﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Servicios;
using RepositorioClases;
using ExperienciaTuristica.Models;
using Datos;

namespace ExperienciaTuristica.Controllers
{
    public class ModeradorController : Controller
    {
        //
        // GET: /Usuarios/
        public ActionResult Index()
        {
            Usuario usu = (Usuario)(Session["Usuario"]);
            if (usu != null)
            {
                if (usu.IdRol == 3)
                {
                    List<DenunciasComentariosModel> denunciasList = new List<DenunciasComentariosModel>();
                    List<DenunciaComentario> denunciasComentarios = DenunciasComentariosService.ObtenerDenuncias(null, null);

                    foreach (var val in denunciasComentarios)
                    {
                        DenunciasComentariosModel denuncia = new DenunciasComentariosModel();
                        denuncia.ComentarioDenunciado = DenunciasComentariosService.ObtenerComentarioPorIdComentario(val.IdComentario).DescripcionComentario;
                        denuncia.IdComentario = val.IdComentario;
                        denuncia.FechaHoraDenuncia = val.FechaHoraDenuncia;
                        denuncia.NombreUsuarioDenunciado = DenunciasComentariosService.ObtenerUsuarioDenunciado(val.IdComentario).NombreUsuario;
                        List<DenunciaComentarioDetalleModel> listaDetalleDenuncia = new List<DenunciaComentarioDetalleModel>();

                        foreach (var det in val.Detalle)
                        {
                            DenunciaComentarioDetalleModel dm = new DenunciaComentarioDetalleModel();
                            dm.IdUsuario = det.IdUsuario;
                            dm.MotivoDenuncia = det.MotivoDenuncia;
                            var usuarioDenunciante = UsuarioServicio.ObtenerUsuario(det.IdUsuario);
                            if (usuarioDenunciante.Count != 0)
                            {
                                dm.NombreUsuarioDenunciante = UsuarioServicio.ObtenerUsuario(det.IdUsuario)[0].NombreUsuario;
                            }
                            else
                            {
                                dm.NombreUsuarioDenunciante = "Este usuario ha sido eliminado.";
                            }
                            listaDetalleDenuncia.Add(dm);
                        }
                        denuncia.Detalle = listaDetalleDenuncia;
                        denunciasList.Add(denuncia);
                    }

                    return View(denunciasList);
                }
                else
                {
                    return RedirectToAction("Error", "General", new { mensaje = "Usted debe ser moderador para ingresar a esta sección." });
                }
            }
            else
            {
                return RedirectToAction("Error", "General", new { mensaje = "Usted debe iniciar sesión." });
            }
        }

        public ActionResult DetalleDenuncia(int idComentario)
        {
            Usuario usu = (Usuario)(Session["Usuario"]);
            if (usu != null)
            {
                if (usu.IdRol == 3)
                {
                    DenunciasComentariosModel denunciasList = new DenunciasComentariosModel();
                    DenunciaComentario denunciasComentarios = DenunciasComentariosService.ObtenerDenunciasPorIdComentario(idComentario);

                    DenunciasComentariosModel denuncia = new DenunciasComentariosModel();
                    denuncia.ComentarioDenunciado = DenunciasComentariosService.ObtenerComentarioPorIdComentario(denunciasComentarios.IdComentario).DescripcionComentario;
                    denuncia.IdComentario = denunciasComentarios.IdComentario;
                    denuncia.FechaHoraDenuncia = denunciasComentarios.FechaHoraDenuncia;
                    denuncia.NombreUsuarioDenunciado = DenunciasComentariosService.ObtenerUsuarioDenunciado(denunciasComentarios.IdComentario).NombreUsuario;
                    List<DenunciaComentarioDetalleModel> listaDetalleDenuncia = new List<DenunciaComentarioDetalleModel>();

                    foreach (var det in denunciasComentarios.Detalle)
                    {
                        DenunciaComentarioDetalleModel dm = new DenunciaComentarioDetalleModel();
                        dm.IdUsuario = det.IdUsuario;
                        dm.MotivoDenuncia = det.MotivoDenuncia;
                        var usuarioDenunciante = UsuarioServicio.ObtenerUsuario(det.IdUsuario);
                        if (usuarioDenunciante.Count != 0)
                        {
                            dm.NombreUsuarioDenunciante = UsuarioServicio.ObtenerUsuario(det.IdUsuario)[0].NombreUsuario;
                        }
                        else
                        {
                            dm.NombreUsuarioDenunciante = "Este usuario ha sido eliminado.";
                        }
                        listaDetalleDenuncia.Add(dm);
                    }
                    denuncia.Detalle = listaDetalleDenuncia;
                    denunciasList = denuncia;

                    return View(denuncia);
                }
                else
                {
                    return RedirectToAction("Error", "General", new { mensaje = "Usted debe ser moderador para ingresar a esta sección." });
                }
            }
            else
            {
                return RedirectToAction("Error", "General", new { mensaje = "Usted debe iniciar sesión." });
            }
        }

        public ActionResult BloquearComentario(int idComentario)
        {
            Usuario usu = (Usuario)(Session["Usuario"]);
            if (usu != null)
            {
                if (usu.IdRol == 3)
                {
                    DenunciasComentariosService.BloquearComentario(idComentario);

                    return RedirectToAction("Index", "Moderador", new { mensaje = "El comentario ha sido bloqueado" });

                }
                else
                {
                    return RedirectToAction("Error", "General", new { mensaje = "Usted debe ser moderador para ingresar a esta sección." });
                }
            }
            else
            {
                return RedirectToAction("Error", "General", new { mensaje = "Usted debe iniciar sesión." });
            }

        }

        public ActionResult RechazarDenuncia(int idComentario)
        {
            Usuario usu = (Usuario)(Session["Usuario"]);
            if (usu != null)
            {
                if (usu.IdRol == 3)
                {                    
                    DenunciasComentariosService.RechazarDenuncia(idComentario);
                    return RedirectToAction("Index", "Moderador", new { mensaje = "Se ha rechazado la denuncia." });
                }
                else
                {
                    return RedirectToAction("Error", "General", new { mensaje = "Usted debe ser moderador para ingresar a esta sección." });
                }
            }
            else
            {
                return RedirectToAction("Error", "General", new { mensaje = "Usted debe iniciar sesión." });
            }
        }
    }
}
