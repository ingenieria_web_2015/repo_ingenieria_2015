﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ExperienciaTuristica.Models;
using Servicios;
using RepositorioClases;
using Datos;
using Microsoft.Web.WebPages.OAuth;
using TweetSharp;
using Facebook;
using Newtonsoft.Json.Linq;

namespace ExperienciaTuristica.Controllers
{
    public class ComunidadController : Controller
    {
        //
        // GET: /Comunidad/

        public ActionResult Index()
        {
            return RedirectToAction("/Comunidad");
        }

        public ActionResult Comunidad()
        {
            ComunidadIndexModel model = new ComunidadIndexModel();
            model.temas = CargarTemas();
            model.galerias = CargarGalerias();
            Usuario usu = (Usuario)(Session["Usuario"]);        
            if (usu != null)
            {
                model.socialUser = new SocialUserModel() { IdFacebook = usu.IdFacebook, IdTwitter = usu.IdTwitter };
            }

            ViewBag.Mensaje = "Pagina usuarios logueados";
            return View(model);
        }

        public List<TemaModel> CargarTemas()
        {
            int index = 0;
            List<Tema> temasClase = TemaService.ObtenerTema(null);
            List<TemaModel> temasModel = new List<TemaModel>();
            foreach (var tem in temasClase)
            {
                if (index < 3)
                {
                    TemaModel tema = new TemaModel();
                    tema.FechaHoraTema = tem.FechaHoraTema;
                    tema.DescripcionTema = Server.HtmlDecode(tem.DescripcionTema);
                    tema.IdCiudad = tem.IdCiudad;
                    tema.IdUsuario = tem.IdUsuario;
                    tema.IdTema = tem.IdTema;
                    tema.TituloTema = tem.TituloTema;
                    tema.IdCategoria = tem.IdCategoria;
                    tema.ContenidoTema = Server.HtmlDecode(tem.ContenidoTema);

                    temasModel.Add(tema);
                    index++;
                }
                else
                {
                    break;
                }

            }
            return temasModel;
        }

        public List<GaleriaModel> CargarGalerias()
        {
            int index = 0;
            List<Galeria> galeriasClase = GaleriaService.ObtenerGaleria(null);
            List<GaleriaModel> galeriasModel = new List<GaleriaModel>();
            foreach (var gal in galeriasClase)
            {
                if (index < 3)
                {
                    GaleriaModel galeria = new GaleriaModel();
                    galeria.FechaHoraGaleria = gal.FechaHoraGaleria;
                    galeria.DescripcionGaleria = Server.HtmlDecode(gal.DescripcionGaleria);
                    galeria.IdCiudad = gal.CiudadId;
                    galeria.IdUsuario = gal.IdUsuario;
                    galeria.IdGaleria = gal.IdGaleria;
                    galeria.TituloGaleria = gal.TituloGaleria;

                    List<Imagen> imagenes = GaleriaService.ObtenerImagenesPorGaleria(galeria.IdGaleria);
                    galeria.imagenesCargadas = imagenes;

                    galeriasModel.Add(galeria);
                    index++;
                }
                else
                {
                    break;
                }
            }
            return galeriasModel;
        }

        public ActionResult ExternalLoginsList(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return PartialView("_ExternalLoginsListPartial", OAuthWebSecurity.RegisteredClientData);
        }

        [HttpGet]
        public JsonResult ObtenerListaAmigosTwitter(string id)
        {
            Usuario usu = (Usuario)(Session["Usuario"]);
            if (usu != null)
            {
                List<UsuarioTwitter> lista = new List<UsuarioTwitter>();
                TwitterService servicioTwitter = new TwitterService("u2dT9aOyGxUzoYTEbwFRgq0gR", "5lB5FMxHc3jbOVYKRGKe0nwDroQoYIgBVIGgXYPhSIOXuyyOVy", "467140253-8swK3rskWPPcfGv4BKmvVNFM9rNie5BBCTAnlGCj", "vawVjjQgyOda1G3ni9O5uo2vYseQ3g83qA1F8O6xGEW6K");
                TwitterCursorList<TwitterUser> friendsList = servicioTwitter.ListFriends(new ListFriendsOptions { Cursor = -1, SkipStatus = true, UserId = Convert.ToInt64(id) });
                if (friendsList.Count > 0)
                {
                    foreach (var item in friendsList)
                    {
                        UsuarioTwitter friend = new UsuarioTwitter();
                        friend.UserName = item.Name;
                        friend.IdTwitter = Convert.ToString(item.Id);
                        lista.Add(friend);
                    }
                }
                return Json(lista, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Error", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult ObtenerListaAmigosFacebook(string id)
        {
            Usuario usu = (Usuario)(Session["Usuario"]);
            if (usu != null)
            {
                UsuarioFacebook uf = UsuarioServicio.ObtenerUsuarioFacebookPorId(usu.IdFacebook);
                string myAccessToken = uf.AccessToken;
                FacebookClient client = new FacebookClient(myAccessToken);

                string[,] friends;
                var friend = client.Get("/me/friends");
                var friend2= client.Get("/me/friends");
                //int count = (int)friend.data.Count;
                //friends = new string[count, 2];
                //for (int i = 0; i < count; i++)
                //{
                //    friends[i, 0] = friend.data[i].id;
                //    friends[i, 1] = friend.data[i].name;
                //}//end for 
                return Json("ok", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Error", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult SendTweetMessage(string id)
        {
            try
            {
                TwitterService servicioTwitter = new TwitterService("u2dT9aOyGxUzoYTEbwFRgq0gR", "5lB5FMxHc3jbOVYKRGKe0nwDroQoYIgBVIGgXYPhSIOXuyyOVy", "467140253-8swK3rskWPPcfGv4BKmvVNFM9rNie5BBCTAnlGCj", "vawVjjQgyOda1G3ni9O5uo2vYseQ3g83qA1F8O6xGEW6K");
                OAuthRequestToken token = servicioTwitter.GetRequestToken();
                
                servicioTwitter.AuthenticateWith(token.Token, token.TokenSecret);

                SendTweetOptions opts = new SendTweetOptions();
                opts.Status = "prueba";
                TwitterStatus status = servicioTwitter.SendTweet(opts);
                
                SendDirectMessageOptions msgOpt = new SendDirectMessageOptions();
                msgOpt.UserId = Convert.ToInt64(id);
                msgOpt.Text = "Prueba envío mensaje";
                servicioTwitter.SendDirectMessage(msgOpt);

                return Json("Ok", JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json("No", JsonRequestBehavior.AllowGet);
            }
        }
    }
}
