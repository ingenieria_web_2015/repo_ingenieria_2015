﻿using Datos;
using ExperienciaTuristica.Models;
using Microsoft.Security.Application;
using RepositorioClases;
using Servicios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Antlr;
using System.IO;
using System.Configuration;

namespace ExperienciaTuristica.Controllers
{
    public class GaleriaController : Controller
    {
        //
        // GET: /Galeria/

        //Va a mostrar todas las galerias
        public ActionResult Index(int? id)
        {
            List<Galeria> galeriasClase = GaleriaService.ObtenerGaleriaPorIdGaleria(id);
            List<GaleriaModel> galeriasModel = new List<GaleriaModel>();
            foreach (var gal in galeriasClase)
            {
                GaleriaModel galeria = new GaleriaModel();
                galeria.FechaHoraGaleria = gal.FechaHoraGaleria;
                galeria.DescripcionGaleria = Server.HtmlDecode(gal.DescripcionGaleria);
                galeria.IdCiudad = gal.CiudadId;
                galeria.NombreCiudad = CiudadServicio.ObtenerCiudadPorIdCiudad(gal.CiudadId).CiudadNombre;
                galeria.IdUsuario = gal.IdUsuario;
                galeria.IdGaleria = gal.IdGaleria;
                galeria.TituloGaleria = gal.TituloGaleria;

                List<Imagen> imagenes = GaleriaService.ObtenerImagenesPorGaleria(galeria.IdGaleria);
                galeria.imagenesCargadas = imagenes;

                galeriasModel.Add(galeria);
            }
            if (galeriasModel.Count != 0)
            {
                ViewBag.Mensaje = "Galerias de experiencia turística:";
            }
            else
            {
                ViewBag.Mensaje = "No existen galerias disponibles.";
            }

            return View(galeriasModel);
        }

        public ActionResult MostrarGaleria(int? id)
        {
            List<Galeria> galerias = GaleriaService.ObtenerGaleriaPorIdGaleria(id);
            List<GaleriaModel> galeriasModel = new List<GaleriaModel>();
            GaleriaModel galeria = new GaleriaModel();
            

            galeria.FechaHoraGaleria = galerias[0].FechaHoraGaleria;
            galeria.DescripcionGaleria = Server.HtmlDecode(galerias[0].DescripcionGaleria);
            galeria.IdCiudad = galerias[0].CiudadId;
            galeria.NombreCiudad = CiudadServicio.ObtenerCiudadPorIdCiudad(galerias[0].CiudadId).CiudadNombre;
            galeria.IdUsuario = galerias[0].IdUsuario;
            galeria.IdGaleria = galerias[0].IdGaleria;
            galeria.TituloGaleria = galerias[0].TituloGaleria;

            List<Imagen> imagenes = GaleriaService.ObtenerImagenesPorGaleria(galeria.IdGaleria);
            galeria.imagenesCargadas = imagenes;

            galeriasModel.Add(galeria);


            return View(galeria);
        }


        public ActionResult MisGalerias()
        {
            Usuario usu = (Usuario)(Session["Usuario"]);
            if (usu != null)
            {
                List<Galeria> galeriasClase = GaleriaService.ObtenerGaleria(usu.IdUsuario);
                List<GaleriaModel> galeriasModel = new List<GaleriaModel>();
                foreach (var gal in galeriasClase)
                {
                    GaleriaModel galeria = new GaleriaModel();
                    galeria.FechaHoraGaleria = gal.FechaHoraGaleria;
                    galeria.DescripcionGaleria = Server.HtmlDecode(gal.DescripcionGaleria);
                    galeria.IdCiudad = gal.CiudadId;
                    galeria.NombreCiudad = CiudadServicio.ObtenerCiudadPorIdCiudad(gal.CiudadId).CiudadNombre;
                    galeria.IdUsuario = gal.IdUsuario;
                    galeria.IdGaleria = gal.IdGaleria;
                    galeria.TituloGaleria = gal.TituloGaleria;
                    galeriasModel.Add(galeria);
                }
                if (galeriasModel.Count != 0)
                {
                    ViewBag.Mensaje = "Estas son sus galerías:";
                }
                else
                {
                    ViewBag.Mensaje = "No tiene disponibles galerías.";
                }

                return View(galeriasModel);
            }
            else
            {
                return RedirectToAction("Error", "General", new { mensaje = "Debe loguearse para acceder a esta sección" });
            }
        }

        public ActionResult CrearGaleria()
        {
            Usuario usu = (Usuario)(Session["Usuario"]);
            if (usu != null)
            {
                GaleriaModel galeria = new GaleriaModel();

                List<Paises> paises = PaisServicio.ObtenerPaises();
                ViewBag.paises = paises;
                List<Ciudades> ciudades = CiudadServicio.ObtenerCiudades();
                ViewBag.ciudades = ciudades;
                return View(galeria);
            }
            else
            {
                return RedirectToAction("Error", "General", new { mensaje = "Debe loguearse para acceder a esta sección" });
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CrearGaleria(GaleriaModel galeria)
        {
            Usuario usu = (Usuario)(Session["Usuario"]);
            if (usu != null)
            {
                Usuario usu2 = UsuarioServicio.ObtenerUsuarioPorNombre(usu.NombreUsuario);

                galeria.IdUsuario = usu2.IdUsuario;
                galeria.FechaHoraGaleria = DateTime.Now;
                galeria.DescripcionGaleria = Server.HtmlEncode(galeria.DescripcionGaleria);

                if (usu.IdRol != 0)
                {
                    if (ModelState.IsValid)
                    {
                        GaleriaService.NuevaGaleria(new RepositorioClases.Galeria()
                        {
                            CiudadId = galeria.IdCiudad,
                            DescripcionGaleria = galeria.DescripcionGaleria,
                            FechaHoraGaleria = galeria.FechaHoraGaleria,
                            TituloGaleria = galeria.TituloGaleria,
                            IdUsuario = galeria.IdUsuario
                        });

                    }
                    return RedirectToAction("MisGalerias");
                }
                else
                {
                    return RedirectToAction("Error", "General");
                }
            }
            else
            {
                return RedirectToAction("Error", "General", new { mensaje = "Debe loguearse para acceder a esta sección" });
            }
        }

        public ActionResult CargarImagenes(int IdGaleria)
        {
            Usuario usu = (Usuario)(Session["Usuario"]);
            if (usu != null)
            {
                ImagenesModel model = new ImagenesModel();
                Session["IdGaleria"] = IdGaleria;
                ViewBag.IdGaleria = IdGaleria;

                List<Imagen> imagenes = GaleriaService.ObtenerImagenesPorGaleria(IdGaleria);
                model.imagenesCargadas = imagenes;

                return View(model);
            }
            else
            {
                return RedirectToAction("Error", "General", new { mensaje = "Debe loguearse para acceder a esta sección" });
            }
        }

        [HttpPost]
        public ActionResult CargarImagenes(ImagenesModel imagen)
        {
            Usuario usu = (Usuario)(Session["Usuario"]);
            if (usu != null)
            {
                int idGaleria = (int)(Session["IdGaleria"]);
                foreach (var archivo in imagen.archivos)
                {
                    if (archivo != null)
                    {
                        if (archivo.ContentLength > 0)
                        {
                            string fileName = Path.GetFileName(archivo.FileName);
                            var pathDB = Path.Combine(ConfigurationManager.AppSettings.Get("PathImagenes"), fileName);
                            var nombreImagen = Path.Combine("Content\\Imagenes\\", fileName);
                            var aux = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, nombreImagen);
                            GaleriaService.GuardarImagen(pathDB, idGaleria);
                            archivo.SaveAs(aux);
                        }
                    }
                }
                return RedirectToAction("MisGalerias");
            }
            else
            {
                return RedirectToAction("Error", "General", new { mensaje = "Debe loguearse para acceder a esta sección" });
            }
        }

        public ActionResult EditarGaleria(int IdGaleria)
        {
            Usuario usu = (Usuario)(Session["Usuario"]);
            if (usu != null)
            {
                Session["IdGaleria"] = IdGaleria;
                List<Galeria> galerias = GaleriaService.ObtenerGaleriaPorIdGaleria(IdGaleria);
                GaleriaModel galeria = new GaleriaModel();

                galeria.DescripcionGaleria = Server.HtmlDecode(galerias[0].DescripcionGaleria);
                galeria.IdCiudad = galerias[0].CiudadId;
                galeria.TituloGaleria = galerias[0].TituloGaleria;

                List<Paises> paises = PaisServicio.ObtenerPaises();
                ViewBag.paises = paises;
                int? idciudad = galeria.IdCiudad;
                if (idciudad == null)
                {
                    idciudad = 0;
                }
                Paises pais = PaisServicio.ObtenerPaisPorIdCiudad(idciudad);
                ViewBag.paisseleccionado = pais.PaisCodigo;
                ViewBag.ciudadseleccionada = idciudad;

                List<Ciudades> ciudades = CiudadServicio.ObtenerCiudades();
                ViewBag.ciudades = ciudades;

                return View(galeria);
            }
            else
            {
                return RedirectToAction("Error", "General", new { mensaje = "Debe loguearse para acceder a esta sección" });
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditarGaleria(GaleriaModel galeria)
        {
            Usuario usu = (Usuario)(Session["Usuario"]);
            if (usu != null)
            {
                int idGaleria = (int)(Session["IdGaleria"]);
                Usuario usu2 = UsuarioServicio.ObtenerUsuarioPorNombre(usu.NombreUsuario);

                galeria.DescripcionGaleria = Server.HtmlEncode(galeria.DescripcionGaleria);
                galeria.IdUsuario = usu2.IdUsuario;
                galeria.FechaHoraGaleria = GaleriaService.ObtenerGaleriaPorIdGaleria(idGaleria)[0].FechaHoraGaleria;
                galeria.FechaEliminacion = null;


                if (usu.IdRol != 0)
                {
                    if (ModelState.IsValid)
                    {
                        GaleriaService.EditarGaleria(new RepositorioClases.Galeria()
                        {
                            CiudadId = galeria.IdCiudad,
                            IdGaleria = idGaleria,
                            FechaHoraGaleria = galeria.FechaHoraGaleria,
                            DescripcionGaleria = galeria.DescripcionGaleria,
                            TituloGaleria = galeria.TituloGaleria,
                        });
                    }
                    return RedirectToAction("MisGalerias");
                }
                else
                {
                    return RedirectToAction("Error", "General");
                }
            }
            else
            {
                return RedirectToAction("Error", "General", new { mensaje = "Debe loguearse para acceder a esta sección" });
            }
        }

        public ActionResult EliminarGaleria(int IdGaleria)
        {
            Usuario usu = (Usuario)(Session["Usuario"]);
            if (usu != null)
            {
                Session["IdGaleria"] = IdGaleria;

                GaleriaService.EliminarGaleria(IdGaleria);

                return RedirectToAction("MisGalerias");
            }
            else
            {
                return RedirectToAction("Error", "General", new { mensaje = "Debe loguearse para acceder a esta sección" });
            }
        }

        [HttpPost]
        public ActionResult ActualizarComentarioImagen(int IdImagen, string descripcion)
        {
            Usuario usu = (Usuario)(Session["Usuario"]);
            if (usu != null)
            {
                bool respuesta = GaleriaService.ActualizarComentarioImagen(IdImagen, descripcion);

                return Json(respuesta, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return RedirectToAction("Error", "General", new { mensaje = "Debe loguearse para acceder a esta sección" });
            }
        }

        [HttpPost]
        public JsonResult EliminarImagen(int IdImagen)
        {
            GaleriaService.EliminarImagen(IdImagen);

            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetCiudadbyPaisCodigo(string PaisCodigo)
        {
            return Json(new SelectList(CiudadServicio.ObtenerCiudades().Where(x => x.PaisCodigo == PaisCodigo).ToList(), "CiudadId", "CiudadNombre"));
        }
    }
}
