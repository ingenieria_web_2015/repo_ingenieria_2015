﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Servicios;
using RepositorioClases;
using ExperienciaTuristica.Models;

namespace ExperienciaTuristica.Controllers
{
    public class BusquedaController : Controller
    {
        //
        // GET: /Busqueda/
        BusquedaService servicioBusqueda = new BusquedaService();

        public JsonResult BuscarTodo(string parametroBusqueda)
        {
            List<ResultadoBusqueda> resultados = servicioBusqueda.BusquedaGeneral(parametroBusqueda);
            return Json(resultados, JsonRequestBehavior.AllowGet);
        }

        public JsonResult BuscarTema(string parametroBusqueda)
        {
            List<ResultadoBusqueda> resultados = servicioBusqueda.BusquedaTema(parametroBusqueda);
            return Json(resultados, JsonRequestBehavior.AllowGet);
        }
        public JsonResult BuscarGaleria(string parametroBusqueda)
        {
            List<ResultadoBusqueda> resultados = servicioBusqueda.BusquedaGaleria(parametroBusqueda);
            return Json(resultados, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult BusquedaCompleta(string titulo, string copete, string descripcion, bool? relacionada)
        {
            string tit = (titulo != null ? tit = titulo : tit = "");
            string cop = (copete != null ? cop = copete : cop = "");
            string desc = (descripcion != null ? desc = descripcion : desc = "");
            bool rel = (relacionada.HasValue ? rel = relacionada.Value : rel = false);
            List<ResultadoBusqueda> resultados = servicioBusqueda.BusquedaAvanzada(tit, cop, desc, rel);

            return View(resultados);
        }
    }
}
