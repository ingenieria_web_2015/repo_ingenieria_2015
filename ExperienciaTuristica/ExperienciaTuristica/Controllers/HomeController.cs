﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Servicios;
using Datos;
using ExperienciaTuristica.Models;
using RepositorioClases;


namespace ExperienciaTuristica.Controllers
{
    public class HomeController : Controller
    {

        public HomeController()
        {
            pais = new Pais();
            ciudad= new Ciudad();
        }

        public ActionResult Index(string PaisCodigo)
        {
            Session["Usuario"] = null;

            List<Paises> paises = PaisServicio.ObtenerPaises();
            ViewBag.paises = paises;

            List<Ciudades> ciudades = CiudadServicio.ObtenerCiudades();
            ViewBag.ciudades = ciudades;


            return RedirectToAction("Index","Comunidad");
        }

        [HttpPost]
        public JsonResult GetCiudadbyPaisCodigo(string PaisCodigo)
        {
            return Json(new SelectList(CiudadServicio.ObtenerCiudades().Where(x => x.PaisCodigo == PaisCodigo).ToList(), "CiudadId", "CiudadNombre"));
        }



        public ActionResult IrComunidad()
        {
            return Redirect("/Comunidad/Index");
        }

        public RepositorioClases.Ciudad ciudad { get; set; }

        public RepositorioClases.Pais pais { get; set; }
    }
}
