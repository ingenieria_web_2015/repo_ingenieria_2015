﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RepositorioClases;
using Servicios;
using System.Security.Cryptography.X509Certificates;
using ExperienciaTuristica.Models;
using Datos;

namespace ExperienciaTuristica.Controllers
{
    public class LoginController : Controller
    {
        //
        // GET: /Login/

        public JsonResult Inicio(string mail, string contraseña)
        {
            string respuesta = "";
            Usuario usu = UsuarioServicio.ObtenerUsuarioPorNombreContraseña(mail, contraseña, 1);

            if (usu == null)
            {
                Session["Usuario"] = null;
                ViewBag.usu = null;
                respuesta = "Otro";
            }
            else
            {
                if (usu.IdRol == 1)
                {
                    respuesta = "IsAdmin";
                }
                else if (usu.IdRol == 2)
                {
                    respuesta = "UsuarioComun";
                }
                else if (usu.IdRol == 3)
                {
                    respuesta = "Moderador";
                }
                else
                {
                    respuesta = "Otro";
                }
                Session["Usuario"] = usu;
                ViewBag.usu = usu;
            }

            return Json(respuesta, JsonRequestBehavior.AllowGet);
        }

        public ActionResult InicioFacebook(UsuarioFacebook usufacebook)
        {
            bool primerLogin = UsuarioServicio.ConsultarIdFacebook(usufacebook.IdFacebook);
            if (primerLogin == false)
            {
                UsuarioServicio.GuardarUsuarioFacebook(usufacebook);

                return RedirectToAction("CompletarRegistroFacebook", new { idFacebook = usufacebook.IdFacebook });
            }
            else
            {
                UsuarioServicio.RelacionarCuentaFacebook(usufacebook);
                Usuario usu = UsuarioServicio.ObtenerUsuarioPorIdFacebook(usufacebook.IdFacebook);
                Session["Usuario"] = usu;
                return RedirectToAction("Index", "Comunidad");
            }
        }

        public ActionResult InicioTwitter(UsuarioTwitter usuTwitter)
        {
            bool primerLogin = UsuarioServicio.ConsultarIdTwitter(usuTwitter.IdTwitter);
            if (primerLogin == false)
            {
                return View(usuTwitter);
            }
            else
            {
                UsuarioTwitter ustw = UsuarioServicio.ObtenerUsuarioTwitterPorId(usuTwitter.IdTwitter);
                UsuarioServicio.RelacionarCuentaTwitter(ustw);
                Usuario usu = UsuarioServicio.ObtenerUsuarioPorIdTwitter(usuTwitter.IdTwitter);
                Session["Usuario"] = usu;
                return RedirectToAction("Index", "Comunidad");
            }
        }

        [HttpPost]
        public ActionResult ConfirmarMailTwitter(UsuarioTwitter usuTwitter)
        {
            UsuarioServicio.GuardarUsuarioTwitter(usuTwitter);
            if (UsuarioServicio.ControlarUsuarioRepetido(usuTwitter.Mail) == false)
            {
                return RedirectToAction("CompletarRegistroTwitter", new { idTwitter = usuTwitter.IdTwitter });
            }
            else
            {
                UsuarioTwitter ustw = UsuarioServicio.ObtenerUsuarioTwitterPorId(usuTwitter.IdTwitter);
                UsuarioServicio.RelacionarCuentaTwitter(ustw);
                Usuario usu = UsuarioServicio.ObtenerUsuarioPorIdTwitter(usuTwitter.IdTwitter);
                Session["Usuario"] = usu;
                return RedirectToAction("Comunidad", "Comunidad");
            }

        }

        public ActionResult CompletarRegistroFacebook(string idFacebook)
        {
            UsuarioFacebook usuFacebook = UsuarioServicio.ObtenerUsuarioFacebookPorId(idFacebook);
            if (UsuarioServicio.ControlarUsuarioRepetido(usuFacebook.Mail) == false)
            {
                UsuarioModel modelo = new UsuarioModel()
                    {
                        NombreApellido = usuFacebook.Name,
                        NombreUsuario = usuFacebook.Name.Replace(" ", ""),
                        Mail = usuFacebook.Mail,
                        IdFacebook = usuFacebook.IdFacebook,
                        Contraseña = Encriptado.Encriptar(UsuarioServicio.GenerarClaveAleatoria())
                    };

                List<Paises> paises = PaisServicio.ObtenerPaises();
                ViewBag.paises = paises;
                List<Ciudades> ciudades = CiudadServicio.ObtenerCiudades();
                ViewBag.ciudades = ciudades;
                return View(modelo);
            }
            else
            {
                UsuarioServicio.RelacionarCuentaFacebook(usuFacebook);
                Usuario usu = UsuarioServicio.ObtenerUsuarioPorIdFacebook(usuFacebook.IdFacebook);
                Session["Usuario"] = usu;
                return RedirectToAction("Comunidad", "Comunidad");
            }
        }

        [HttpPost]
        public ActionResult CompletarRegistroFacebook(UsuarioModel usuario)
        {
            usuario.IdEstado = 1;
            usuario.IdRol = 2;

            UsuarioServicio.NuevoUsuario(new RepositorioClases.Usuario()
            {
                Contraseña = usuario.Contraseña,
                Direccion = usuario.Direccion,
                FechaNacimiento = usuario.FechaNacimiento,
                IdEstado = usuario.IdEstado,
                IdCiudad = usuario.IdCiudad,
                IdRol = usuario.IdRol,
                Mail = usuario.Mail,
                NombreUsuario = usuario.NombreUsuario,
                NombreApellido = usuario.NombreApellido,
                IdFacebook = usuario.IdFacebook
            });

            usuario.Contraseña = Encriptado.DesEncriptar(usuario.Contraseña);
            Usuario usu = UsuarioServicio.ObtenerUsuarioPorNombreContraseña(usuario.Mail, usuario.Contraseña, 1);
            Session["Usuario"] = usu;

            return RedirectToAction("Comunidad", "Comunidad");
        }

        public ActionResult CompletarRegistroTwitter(string idTwitter)
        {
            UsuarioTwitter usuTwitter = UsuarioServicio.ObtenerUsuarioTwitterPorId(idTwitter);
            if (UsuarioServicio.ControlarUsuarioRepetido(usuTwitter.Mail) == false)
            {
                UsuarioModel modelo = new UsuarioModel()
                {
                    NombreUsuario = usuTwitter.UserName,
                    Mail = usuTwitter.Mail,
                    IdTwitter = usuTwitter.IdTwitter,
                    Contraseña = Encriptado.Encriptar(UsuarioServicio.GenerarClaveAleatoria())
                };

                List<Paises> paises = PaisServicio.ObtenerPaises();
                ViewBag.paises = paises;
                List<Ciudades> ciudades = CiudadServicio.ObtenerCiudades();
                ViewBag.ciudades = ciudades;
                return View(modelo);
            }
            else
            {
                UsuarioServicio.RelacionarCuentaTwitter(usuTwitter);
                Usuario usu = UsuarioServicio.ObtenerUsuarioPorIdTwitter(usuTwitter.IdTwitter);
                Session["Usuario"] = usu;
                return RedirectToAction("Comunidad", "Comunidad");
            }
        }

        [HttpPost]
        public ActionResult CompletarRegistroTwitter(UsuarioModel usuario)
        {
            usuario.IdEstado = 1;
            usuario.IdRol = 2;

            UsuarioServicio.NuevoUsuario(new RepositorioClases.Usuario()
            {
                Contraseña = usuario.Contraseña,
                Direccion = usuario.Direccion,
                FechaNacimiento = usuario.FechaNacimiento,
                IdEstado = usuario.IdEstado,
                IdCiudad = usuario.IdCiudad,
                IdRol = usuario.IdRol,
                Mail = usuario.Mail,
                NombreUsuario = usuario.NombreUsuario,
                NombreApellido = usuario.NombreApellido,
                IdTwitter = usuario.IdTwitter
            });

            usuario.Contraseña = Encriptado.DesEncriptar(usuario.Contraseña);
            Usuario usu = UsuarioServicio.ObtenerUsuarioPorNombreContraseña(usuario.Mail, usuario.Contraseña, 1);
            Session["Usuario"] = usu;

            return RedirectToAction("Comunidad", "Comunidad");
        }

        public ActionResult CerrarSesion()
        {
            Session.Abandon();
            Session["Usuario"] = null;

            return Redirect("/Home/Index");
        }

    }
}
