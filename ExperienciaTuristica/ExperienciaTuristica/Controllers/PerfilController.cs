﻿using Servicios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RepositorioClases;
using System.Web.Mvc;
using ExperienciaTuristica.Models;
using System.IO;
using System.Configuration;
using System.Drawing;
using Datos;

namespace ExperienciaTuristica.Controllers
{
    public class PerfilController : Controller
    {
        //
        // GET: /Perfil/

        public ActionResult Index()
        {
            Usuario usu = (Usuario)(Session["Usuario"]);
            if (usu != null)
            {
                PerfilModel perfil = new PerfilModel();
                perfil.IdCiudad = usu.IdCiudad;
                perfil.NombreCiudad = CiudadServicio.ObtenerCiudadPorIdCiudad(usu.IdCiudad).CiudadNombre;
                perfil.IdUsuario = usu.IdUsuario;
                perfil.Mail = usu.Mail;
                perfil.NombreUsuario = usu.NombreUsuario;
                perfil.NombreApellido = usu.NombreApellido;
                perfil.FotoPerfil = ConfigurationManager.AppSettings.Get("PathImagenes") + usu.FotoPerfil;
                perfil.Direccion = usu.Direccion;
                perfil.FechaNacimiento = usu.FechaNacimiento;
                

                return View(perfil);
            }
            else
            {
                return RedirectToAction("Error", "General", new { mensaje = "Debe loguearse para ver los perfiles de los usuarios." });
            }
        }
        
        [HttpPost]
        public ActionResult Index(ImagenModel imagen)
        {
            Usuario usu = (Usuario)(Session["Usuario"]);
            if (usu != null)
            {

                if (imagen.File.ContentLength > 0)
                {
                    string fileName = Path.GetFileName(imagen.File.FileName);
                    var pathDB = Path.Combine(ConfigurationManager.AppSettings.Get("PathImagenes"), fileName);
                    var nombreImagen = Path.Combine("Content\\Imagenes\\", fileName);
                    var aux = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, nombreImagen);
                    imagen.File.SaveAs(aux);

                    Usuario usuarios = (Usuario)(Session["Usuario"]);
                    usuarios.FotoPerfil = fileName;
                    Session["Usuario"] = usuarios;
                    PerfilService.GuardarPerfilUsuario(usuarios, fileName);

                    PerfilModel perfil = new PerfilModel();
                    perfil.IdCiudad = usuarios.IdCiudad;
                    perfil.IdUsuario = usuarios.IdUsuario;
                    perfil.Mail = usuarios.Mail;
                    perfil.NombreUsuario = usuarios.NombreUsuario;
                    perfil.NombreApellido = usuarios.NombreApellido;
                    perfil.FotoPerfil = pathDB;

                    return View(perfil);
                }
                return RedirectToAction("/Index");
            }
            else
            {
                return RedirectToAction("Error", "General", new { mensaje = "Debe estar logueado para acceder a esta sección" });
            }
        }

        public ActionResult VerPerfil(int id)
        {
            Usuario usu = (Usuario)(Session["Usuario"]);
            if (usu != null)
            {
                RepositorioClases.Usuario usuario = PerfilService.ObtenerUsuario(id);

                PerfilModel perfil = new PerfilModel();
                perfil.IdCiudad = usuario.IdCiudad;
                perfil.NombreCiudad = CiudadServicio.ObtenerCiudadPorIdCiudad(usuario.IdCiudad).CiudadNombre;
                perfil.IdUsuario = usuario.IdUsuario;
                perfil.Mail = usuario.Mail;
                perfil.NombreUsuario = usuario.NombreUsuario;
                perfil.NombreApellido = usuario.NombreApellido;
                if (usuario.FotoPerfil != null)
                {
                    perfil.FotoPerfil = Path.Combine(ConfigurationManager.AppSettings.Get("PathImagenes"), usuario.FotoPerfil);
                }
                return View(perfil);
            }
            else
            {
                return RedirectToAction("Error", "General", new { mensaje = "Debe loguearse para ver los perfiles de los usuarios." });
            }
        }
        public ActionResult EditarPerfil(int id)
        {
            Usuario usu = (Usuario)(Session["Usuario"]);
            if (usu != null)
            {
                if (usu.IdUsuario == id)
                {
                    List<RepositorioClases.Usuario> usuarios = UsuarioServicio.ObtenerUsuario(id);
                    UsuarioModel usuario = new UsuarioModel();

                    usuario.Direccion = usuarios[0].Direccion;
                    usuario.FechaNacimiento = usuarios[0].FechaNacimiento;
                    usuario.IdCiudad = usuarios[0].IdCiudad;
                    usuario.IdUsuario = usuarios[0].IdUsuario;
                    usuario.Mail = usuarios[0].Mail;
                    usuario.NombreUsuario = usuarios[0].NombreUsuario;
                    usuario.NombreApellido = usuarios[0].NombreApellido;

                    List<Paises> paises = PaisServicio.ObtenerPaises();
                    ViewBag.paises = paises;
                    int? idciudad = usuario.IdCiudad;
                    if (idciudad == null)
                    {
                        idciudad = 0;
                    }
                    Paises pais = PaisServicio.ObtenerPaisPorIdCiudad(idciudad);
                    ViewBag.paisseleccionado = pais.PaisCodigo;
                    ViewBag.ciudadseleccionada = idciudad;

                    List<Ciudades> ciudades = CiudadServicio.ObtenerCiudades();
                    ViewBag.ciudades = ciudades;

                    List<Estados> estados = EstadoServicio.ObtenerEstados();
                    ViewBag.estados = estados;

                    return View(usuario);
        }
                else
                {
                    return RedirectToAction("Error", "General", new { mensaje = "Usted debe iniciar sesión." });
                }
            }
            else
            {
                return RedirectToAction("Error", "General", new { mensaje = "Usted debe iniciar sesión." });
            }
        }

        [HttpPost]
        public ActionResult EditarPerfil(UsuarioModel usuario)
        {
            Usuario usu = (Usuario)(Session["Usuario"]);
            if (usu != null)
            {
                if (usu.IdUsuario == usuario.IdUsuario)
                {

                    PerfilService.EditarPerfil(new RepositorioClases.Usuario()
                    {
                        Direccion = usuario.Direccion,
                        FechaNacimiento = usuario.FechaNacimiento,
                        IdCiudad = usuario.IdCiudad,
                        IdUsuario = usuario.IdUsuario,
                        Mail = usuario.Mail,
                        NombreUsuario = usuario.NombreUsuario,
                        NombreApellido = usuario.NombreApellido,
                    });
                    Usuario us = UsuarioServicio.ObtenerUsuarioPorNombreContraseña(usu.NombreUsuario, Encriptado.DesEncriptar(usu.Contraseña), 1);
                    Session["Usuario"] = us;
                    return RedirectToAction("Index");

                }
                else
                {
                    return RedirectToAction("Error", "General", new { mensaje = "Usted debe iniciar sesión." });
                }
            }
            else
            {
                return RedirectToAction("Error", "General", new { mensaje = "Usted debe iniciar sesión." });
            }
        }

    }
         
}
