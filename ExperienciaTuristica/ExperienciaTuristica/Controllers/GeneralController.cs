﻿using Datos;
using ExperienciaTuristica.Models;
using RepositorioClases;
using Servicios;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.IO;
using System.Web.Mvc;

namespace ExperienciaTuristica.Controllers
{
    public class GeneralController : Controller
    {
        public ActionResult Registrar()
        {
            List<Paises> paises = PaisServicio.ObtenerPaises();
            ViewBag.paises = paises;

            List<Ciudades> ciudades = CiudadServicio.ObtenerCiudades();
            ViewBag.ciudades = ciudades;
            return View();

        }

        [HttpPost]
        public ActionResult Registrar(UsuarioModel usuario)
        {
            usuario.IdEstado = 4;
            usuario.IdRol = 2;

            if (UsuarioServicio.ControlarUsuarioRepetido(usuario.Mail) == false)
            {
                UsuarioServicio.NuevoUsuario(new RepositorioClases.Usuario()
                {
                    Contraseña = Encriptado.Encriptar(usuario.Contraseña),
                    Direccion = usuario.Direccion,
                    FechaNacimiento = usuario.FechaNacimiento,
                    IdEstado = usuario.IdEstado,
                    IdCiudad = usuario.IdCiudad,
                    IdRol = usuario.IdRol,
                    IdUsuario = usuario.IdUsuario,
                    Mail = usuario.Mail,
                    NombreUsuario = usuario.NombreUsuario,
                    NombreApellido = usuario.NombreApellido
                });

                string usu = usuario.Mail;
                string cont = usuario.Contraseña;
                int userId = UsuarioServicio.ObtenerUsuarioPorNombreContraseña(usu, cont, 4).IdUsuario;

                var fullUrl = ConfigurationManager.AppSettings.Get("UrlPrincipal");
                fullUrl = fullUrl + "General/ConfirmarMail?id=" + userId;

                bool resultado = EnvioService.EnviarEmail(usuario.Mail, fullUrl);

                return Redirect("/Home/Index");
            }

            return Redirect("/Home/Index");
        }

        public ActionResult ConfirmarMail(int id)
        {
            CaptchaModel cp = new CaptchaModel()
            {
                id = id
            };
            return View(new CaptchaModel());
        }

        [HttpGet]
        public JsonResult ControlarUsuarioRepetido(string mail)
        {
            bool resp = UsuarioServicio.ControlarUsuarioRepetido(mail);
            return Json(resp, JsonRequestBehavior.AllowGet);
        }


        public ActionResult ConfirmarContra(int id)
        {
            CaptchaModel cp = new CaptchaModel()
            {
                id = id
            };
            return View(new CaptchaModel());
        }

        [HttpPost]
        public JsonResult ConfirmarContra(string clave, int id)
        {
            return Json(UsuarioServicio.CambiarContrasena(clave, id), JsonRequestBehavior.AllowGet);
        }

        public ActionResult EnviarMailCambioContra()
        {
            return View();
        }

        [HttpPost]
        public JsonResult EnviarMailCambioContra(string mail)
        {
            Usuario usuario = UsuarioServicio.ObtenerUsuarioPorEmail(mail);
            if (usuario != null)
            {
                var fullUrl = ConfigurationManager.AppSettings.Get("UrlPrincipal");
                fullUrl = fullUrl + "General/ConfirmarContra?id=" + usuario.IdUsuario;

                bool resultado = EnvioService.EnviarEmail(usuario.Mail, fullUrl);
                return Json("Ok", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Error", JsonRequestBehavior.AllowGet);
            }

        }

        [HttpGet]
        public JsonResult ValidarCaptchaContra(CaptchaModel model)
        {
            //validate captcha
            if (Session["Captcha"] == null || Session["Captcha"].ToString() != model.Captcha)
            {
                return Json("Error", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("Ok", JsonRequestBehavior.AllowGet);

            }
        }

        [HttpGet]
        public JsonResult ValidarCaptcha(CaptchaModel model)
        {
            //validate captcha
            if (Session["Captcha"] == null || Session["Captcha"].ToString() != model.Captcha)
            {
                return Json("Error", JsonRequestBehavior.AllowGet);
            }
            else
            {
                bool resp = UsuarioServicio.ConfirmarMail(model.id);
                if (resp == true)
                {
                    return Json("Ok", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Error2", JsonRequestBehavior.AllowGet);
                }
            }
        }

        public ActionResult CaptchaImage(string prefix, bool noisy = true)
        {
            var rand = new Random((int)DateTime.Now.Ticks);
            //generate new question
            int a = rand.Next(10, 99);
            int b = rand.Next(0, 9);
            var captcha = string.Format("{0} + {1} = ?", a, b);

            //store answer
            Session["Captcha" + prefix] = a + b;

            //image stream
            FileContentResult img = null;

            using (var mem = new MemoryStream())
            using (var bmp = new Bitmap(130, 30))
            using (var gfx = Graphics.FromImage((Image)bmp))
            {
                gfx.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
                gfx.SmoothingMode = SmoothingMode.AntiAlias;
                gfx.FillRectangle(Brushes.White, new Rectangle(0, 0, bmp.Width, bmp.Height));

                //add noise
                if (noisy)
                {
                    int i, r, x, y;
                    var pen = new Pen(Color.Yellow);
                    for (i = 1; i < 10; i++)
                    {
                        pen.Color = Color.FromArgb(
                        (rand.Next(0, 255)),
                        (rand.Next(0, 255)),
                        (rand.Next(0, 255)));

                        r = rand.Next(0, (130 / 3));
                        x = rand.Next(0, 130);
                        y = rand.Next(0, 30);

                        gfx.DrawEllipse(pen, x - r, y - r, r, r);
                    }
                }

                //add question
                gfx.DrawString(captcha, new Font("Tahoma", 15), Brushes.Gray, 2, 3);

                //render as Jpeg
                bmp.Save(mem, System.Drawing.Imaging.ImageFormat.Jpeg);
                img = this.File(mem.GetBuffer(), "image/Jpeg");
            }

            return img;
        }

        public ActionResult Error(string mensaje)
        {
            ViewBag.Mensaje = mensaje;
            return View();
        }

        public ActionResult PrivacyPolicy()
        {
            return View();
        }
    }
}
