﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Servicios;
using RepositorioClases;
using ExperienciaTuristica.Models;
using Datos;

namespace ExperienciaTuristica.Controllers
{
    public class UsuariosController : Controller
    {
        //
        // GET: /Usuarios/
        public ActionResult Index()
        {
            Usuario usu = (Usuario)(Session["Usuario"]);
            if (usu != null)
            {
                if (usu.IdRol == 1)
                {
                    List<RepositorioClases.Usuario> usuarios = UsuarioServicio.ObtenerUsuario(null);
                    List<UsuarioModel> usuariosModels = new List<UsuarioModel>();

                    foreach (RepositorioClases.Usuario u in usuarios)
                    {
                        UsuarioModel usuario = new UsuarioModel();
                        usuario.Direccion = u.Direccion;
                        usuario.FechaNacimiento = u.FechaNacimiento;
                        usuario.IdEstado = u.IdEstado;
                        usuario.IdCiudad = u.IdCiudad;
                        usuario.IdRol = u.IdRol;
                        usuario.IdUsuario = u.IdUsuario;
                        usuario.Mail = u.Mail;
                        usuario.NombreUsuario = u.NombreUsuario;
                        usuario.NombreApellido = u.NombreApellido;
                        usuario.NombreCiudad = CiudadServicio.ObtenerCiudadPorIdCiudad(u.IdCiudad).CiudadNombre;
                        usuario.DescripcionRol = RolServicio.ObtenerRolPorIdRol(u.IdRol).DescripcionRol;
                        usuario.DescripcionBloqueo = EstadoServicio.ObtenerEstadoPorIdEstado(u.IdEstado).DescripcionBloqueo;

                        usuariosModels.Add(usuario);
                    }

                    ViewBag.NombreUsuario = (string)(Session["NombreUsuario"]);

                    return View(usuariosModels);
                }
                else
                {
                    return RedirectToAction("Error", "General", new { mensaje = "Usted debe iniciar sesión como administrador." });
                }
            }
            else
            {
                return RedirectToAction("Error", "General", new { mensaje = "Usted debe iniciar sesión." });
            }
        }

        public ActionResult DetalleUsuario(int id)
        {
            Usuario usu = (Usuario)(Session["Usuario"]);
            if (usu != null)
            {
                if (usu.IdRol == 1)
                {
                    List<RepositorioClases.Usuario> usuarios = UsuarioServicio.ObtenerUsuario(id);
                    UsuarioModel usuario = new UsuarioModel();

                    usuario.Direccion = usuarios[0].Direccion;
                    usuario.FechaNacimiento = usuarios[0].FechaNacimiento;
                    usuario.IdEstado = usuarios[0].IdEstado;
                    usuario.IdCiudad = usuarios[0].IdCiudad;
                    usuario.IdRol = usuarios[0].IdRol;
                    usuario.IdUsuario = usuarios[0].IdUsuario;
                    usuario.Mail = usuarios[0].Mail;
                    usuario.NombreUsuario = usuarios[0].NombreUsuario;
                    usuario.NombreApellido = usuarios[0].NombreApellido;
                    usuario.NombreCiudad = CiudadServicio.ObtenerCiudadPorIdCiudad(usuarios[0].IdCiudad).CiudadNombre;
                    usuario.DescripcionRol = RolServicio.ObtenerRolPorIdRol(usuarios[0].IdRol).DescripcionRol;
                    usuario.DescripcionBloqueo = EstadoServicio.ObtenerEstadoPorIdEstado(usuarios[0].IdEstado).DescripcionBloqueo;

                    return View(usuario);
                }
                else
                {
                    return RedirectToAction("Error", "General", new { mensaje = "Usted debe iniciar sesión como administrador." });
                }
            }
            else
            {
                return RedirectToAction("Error", "General", new { mensaje = "Usted debe iniciar sesión." });
            }
        }

        //Nuevo Usuario

        public ActionResult CrearUsuario()
        {
            Usuario usu = (Usuario)(Session["Usuario"]);
            if (usu != null)
            {
                if (usu.IdRol == 1)
                {
                    UsuarioModel usuario = new UsuarioModel();

                    List<Paises> paises = PaisServicio.ObtenerPaises();
                    ViewBag.paises = paises;

                    List<Ciudades> ciudades = CiudadServicio.ObtenerCiudades();
                    ViewBag.ciudades = ciudades;

                    List<Estados> estados = EstadoServicio.ObtenerEstados();
                    ViewBag.estados = estados;

                    List<Roles> roles = RolServicio.ObtenerRoles();
                    ViewBag.roles = roles;
                    return View(usuario);
                }
                else
                {
                    return RedirectToAction("Error", "General", new { mensaje = "Usted debe iniciar sesión como administrador." });
                }
            }
            else
            {
                return RedirectToAction("Error", "General", new { mensaje = "Usted debe iniciar sesión." });
            }
        }

        [HttpPost]
        public JsonResult CrearUsuario(UsuarioModel usuario)
        {
            Usuario usu = (Usuario)(Session["Usuario"]);
            if (usu.IdRol == 1)
            {
                if (ModelState.IsValid)
                {
                    if (UsuarioServicio.ControlarUsuarioRepetido(usuario.Mail) == false)
                    {
                        UsuarioServicio.NuevoUsuario(new RepositorioClases.Usuario()
                        {
                            Contraseña = Encriptado.Encriptar(usuario.Contraseña),
                            Direccion = usuario.Direccion,
                            FechaNacimiento = usuario.FechaNacimiento,
                            IdEstado = 1,
                            IdCiudad = usuario.IdCiudad,
                            IdRol = usuario.IdRol,
                            IdUsuario = usuario.IdUsuario,
                            Mail = usuario.Mail,
                            NombreUsuario = usuario.NombreUsuario,
                            NombreApellido = usuario.NombreApellido,
                        });
                        return Json("Correcto", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json("ErrorUsuarioRepetido", JsonRequestBehavior.AllowGet);
                    }
                }
                return Json("ErrorModelo", JsonRequestBehavior.AllowGet);
            }
            return Json("ErrorPermiso", JsonRequestBehavior.AllowGet);
        }

        public ActionResult EditarUsuario(int id)
        {
            Usuario usu = (Usuario)(Session["Usuario"]);
            if (usu != null)
            {
                if (usu.IdRol == 1)
                {
                    List<RepositorioClases.Usuario> usuarios = UsuarioServicio.ObtenerUsuario(id);
                    UsuarioModel usuario = new UsuarioModel();

                    usuario.Contraseña = usuarios[0].Contraseña;
                    usuario.Direccion = usuarios[0].Direccion;
                    usuario.FechaNacimiento = usuarios[0].FechaNacimiento;
                    usuario.IdEstado = usuarios[0].IdEstado;
                    usuario.IdCiudad = usuarios[0].IdCiudad;
                    usuario.IdRol = usuarios[0].IdRol;
                    usuario.IdUsuario = usuarios[0].IdUsuario;
                    usuario.Mail = usuarios[0].Mail;
                    usuario.NombreUsuario = usuarios[0].NombreUsuario;
                    usuario.NombreApellido = usuarios[0].NombreApellido;
                    usuario.FotoPerfil = usuarios[0].FotoPerfil;

                    List<Paises> paises = PaisServicio.ObtenerPaises();
                    ViewBag.paises = paises;
                    int? idciudad = usuario.IdCiudad;
                    if (idciudad == null)
                    {
                        idciudad = 0;
                    }
                    Paises pais = PaisServicio.ObtenerPaisPorIdCiudad(idciudad);
                    ViewBag.paisseleccionado = pais.PaisCodigo;
                    ViewBag.ciudadseleccionada = idciudad;

                    List<Ciudades> ciudades = CiudadServicio.ObtenerCiudades();
                    ViewBag.ciudades = ciudades;

                    List<Estados> estados = EstadoServicio.ObtenerEstados();
                    ViewBag.estados = estados;

                    return View(usuario);
                }
                else
                {
                    return RedirectToAction("Error", "General", new { mensaje = "Usted debe iniciar sesión como administrador." });
                }
            }
            else
            {
                return RedirectToAction("Error", "General", new { mensaje = "Usted debe iniciar sesión." });
            }
        }

        [HttpPost]
        public ActionResult EditarUsuario(UsuarioModel usuario)
        {
            Usuario usu = (Usuario)(Session["Usuario"]);
            if (usu != null)
            {
                if (usu.IdRol == 1)
                {
                    if (ModelState.IsValid)
                    {
                        UsuarioServicio.EditarUsuario(new RepositorioClases.Usuario()
                        {
                            Contraseña = usuario.Contraseña,
                            Direccion = usuario.Direccion,
                            FechaNacimiento = usuario.FechaNacimiento,
                            IdEstado = usuario.IdEstado,
                            IdCiudad = usuario.IdCiudad,
                            IdRol = usuario.IdRol,
                            IdUsuario = usuario.IdUsuario,
                            Mail = usuario.Mail,
                            NombreUsuario = usuario.NombreUsuario,
                            NombreApellido = usuario.NombreApellido,
                            FotoPerfil = usuario.FotoPerfil
                        });

                        return RedirectToAction("Index");
                    }
                    return View(usuario);
                }
                else
                {
                    return RedirectToAction("Error", "General", new { mensaje = "Usted debe iniciar sesión como administrador." });
                }
            }
            else
            {
                return RedirectToAction("Error", "General", new { mensaje = "Usted debe iniciar sesión." });
            }
        }

        public ActionResult EliminarUsuario(int id)
        {
            Usuario usu = (Usuario)(Session["Usuario"]);
            if (usu != null)
            {
                if (usu.IdRol == 1)
                {
                    List<RepositorioClases.Usuario> usuarios = UsuarioServicio.ObtenerUsuario(id);
                    UsuarioModel usuario = new UsuarioModel();

                    usuario.Direccion = usuarios[0].Direccion;
                    usuario.FechaNacimiento = usuarios[0].FechaNacimiento;
                    usuario.IdEstado = usuarios[0].IdEstado;
                    usuario.IdCiudad = usuarios[0].IdCiudad;
                    usuario.IdRol = usuarios[0].IdRol;
                    usuario.IdUsuario = usuarios[0].IdUsuario;
                    usuario.Mail = usuarios[0].Mail;
                    usuario.NombreUsuario = usuarios[0].NombreUsuario;
                    usuario.NombreApellido = usuarios[0].NombreApellido;
                    usuario.FotoPerfil = usuarios[0].FotoPerfil;

                    return View(usuario);
                }
                else
                {
                    return RedirectToAction("Error", "General", new { mensaje = "Usted debe iniciar sesión como administrador." });
                }
            }
            else
            {
                return RedirectToAction("Error", "General", new { mensaje = "Usted debe iniciar sesión." });
            }
        }

        [HttpPost, ActionName("EliminarUsuario")]
        public ActionResult ConfirmacionEliminarUsuario(int id)
        {
            Usuario usu = (Usuario)(Session["Usuario"]);
            if (usu != null)
            {
                if (usu.IdRol == 1)
                {
                    UsuarioServicio.EliminarUsuario(new RepositorioClases.Usuario()
                    {
                        IdUsuario = id,
                        IdEstado = 4
                    });

                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("Error", "General", new { mensaje = "Usted debe iniciar sesión como administrador." });
                }
            }
            else
            {
                return RedirectToAction("Error", "General", new { mensaje = "Usted debe iniciar sesión." });
            }
        }

        [HttpPost]
        public JsonResult GetCiudadbyPaisCodigo(string PaisCodigo)
        {
            return Json(new SelectList(CiudadServicio.ObtenerCiudades().Where(x => x.PaisCodigo == PaisCodigo).ToList(), "CiudadId", "CiudadNombre"));
        }
    }
}
