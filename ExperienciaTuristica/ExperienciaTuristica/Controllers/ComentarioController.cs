﻿using ExperienciaTuristica.Models;
using RepositorioClases;
using Servicios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ExperienciaTuristica.Controllers
{
    public class ComentarioController : Controller
    {
        //
        // GET: /Comentario/

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult CrearComentario(string descripcionComentario, int idTema) {
            bool resultado;
            Usuario usu = (Usuario)(Session["Usuario"]);
            if (usu != null)
            {                
                if (descripcionComentario != "")
                {
                    resultado = TemaService.GuardarComentario(descripcionComentario, idTema, usu.IdUsuario);
                }
                else
                {
                    resultado = false;
                }

                return Json(resultado, JsonRequestBehavior.AllowGet);
            }
            else
            {
                resultado = false;
                return Json(resultado, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult EditarComentario(int idComentario, string descripcion)
        {
            bool resultado;
            Usuario usu = (Usuario)(Session["Usuario"]);
            if (usu != null)
            {
                Comentario c = TemaService.ObtenerComentario(idComentario);
                c.DescripcionComentario = descripcion;
                resultado = TemaService.EditarComentario(c);

                return Json(resultado, JsonRequestBehavior.AllowGet);
            }
            else
            {
                resultado = false;
                return Json(resultado, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult CrearDenuncia(int idComentario, string motivo)
        {
            bool resultado;
            Usuario usu = (Usuario)(Session["Usuario"]);
            if (usu != null)
            {
                resultado = ComentarioService.GuardarDenuncia(idComentario, usu.IdUsuario, motivo);
                return Json(resultado, JsonRequestBehavior.AllowGet);
            }
            else
            {
                resultado = false;
                return Json(resultado, JsonRequestBehavior.AllowGet);
            }

        }
        
        public JsonResult EliminarComentario(int idComentario)
        {
            bool resultado;
            Usuario usu = (Usuario)(Session["Usuario"]);
            if (usu != null)
            {
                resultado = TemaService.EliminarComentario(idComentario);
                return Json(resultado, JsonRequestBehavior.AllowGet);
            }
            else
            {
                resultado = false;
                return Json(resultado, JsonRequestBehavior.AllowGet);
            }
        }

    }
}
