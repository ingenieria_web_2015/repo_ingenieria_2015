﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Datos;
using ExperienciaTuristica.Models;
using Servicios;
using RepositorioClases;
using System.Text.RegularExpressions;
using Facebook;
using System.Dynamic;
using Tweetinvi;
using Tweetinvi.Controllers;
using TweetSharp;
namespace ExperienciaTuristica.Controllers
{
    public class TemaController : Controller
    {
        //
        // GET: /Tema/
       
         
        public ActionResult Index()
        {
            Usuario usu = (Usuario)(Session["Usuario"]);
            List<Tema> temasClase = TemaService.ObtenerTema(null);
            List<TemaModel> temasModel = new List<TemaModel>();
            foreach (var tem in temasClase)
            {
                TemaModel tema = new TemaModel();
                tema.FechaHoraTema = tem.FechaHoraTema;
                tema.DescripcionTema = Server.HtmlDecode(tem.DescripcionTema);
                tema.IdCiudad = tem.IdCiudad;
                tema.NombreCiudad = CiudadServicio.ObtenerCiudadPorIdCiudad(tem.IdCiudad).CiudadNombre;
                tema.IdUsuario = tem.IdUsuario;
                tema.IdTema = tem.IdTema;
                tema.TituloTema = tem.TituloTema;
                tema.IdCategoria = tem.IdCategoria;
                tema.ContenidoTema = Server.HtmlDecode(tem.ContenidoTema);

                temasModel.Add(tema);
            }
            
            List<CategoriasTemas> categorias = TemaService.ObtenerCategorias(null);
            ViewBag.listaCategorias = categorias;

            if (temasModel.Count != 0)
            {
                ViewBag.Mensaje = "Temas de experiencia turística:";
            }
            else
            {
                ViewBag.Mensaje = "No existen temas disponibles.";
            }

            return View(temasModel);
        }

        public ActionResult MisTemas()
        {
            Usuario usu = (Usuario)(Session["Usuario"]);
            if (usu != null)
            {
                List<Tema> temasClase = TemaService.ObtenerTema(usu.IdUsuario);
                List<TemaModel> temasModel = new List<TemaModel>();
                foreach (var tem in temasClase)
                {
                    TemaModel tema = new TemaModel();
                    tema.FechaHoraTema = tem.FechaHoraTema;
                    tema.DescripcionTema = Server.HtmlDecode(tem.DescripcionTema);
                    tema.IdCiudad = tem.IdCiudad;
                    tema.NombreCiudad = CiudadServicio.ObtenerCiudadPorIdCiudad(tem.IdCiudad).CiudadNombre;
                    tema.IdUsuario = tem.IdUsuario;
                    tema.IdTema = tem.IdTema;
                    tema.TituloTema = tem.TituloTema;
                    tema.ContenidoTema = Server.HtmlDecode(tem.ContenidoTema);
                    tema.IdCategoria = tem.IdCategoria;
                    temasModel.Add(tema);
                }
                if (temasModel.Count != 0)
                {
                    ViewBag.Mensaje = "Estos son sus temas:";
                }
                else
                {
                    ViewBag.Mensaje = "No tiene temas disponibles.";
                }
                return View(temasModel);
            }
            else
            {
                return RedirectToAction("Error", "General", new { mensaje = "Debe loguearse para acceder a esta sección" });
            }
        }

        public ActionResult CrearTema()
        {
            Usuario usu = (Usuario)(Session["Usuario"]);
            if (usu != null)
            {
                TemaModel tema = new TemaModel();

                List<Paises> paises = PaisServicio.ObtenerPaises();
                ViewBag.paises = paises;
                List<Ciudades> ciudades = CiudadServicio.ObtenerCiudades();
                ViewBag.ciudades = ciudades;
                List<CategoriasTemas> categorias = TemaService.ObtenerCategorias(null);
                ViewBag.categorias = categorias;
                List<Galeria> galerias = GaleriaService.ObtenerGaleria(usu.IdUsuario);
                ViewBag.galerias = galerias;
                return View(tema);
            }
            else
            {
                return RedirectToAction("Error", "General", new { mensaje = "Debe loguearse para acceder a esta sección" });
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CrearTema(TemaModel tema)
        {
            Usuario usu = (Usuario)(Session["Usuario"]);
            if (usu != null)
            {
                Usuario usu2 = UsuarioServicio.ObtenerUsuarioPorNombre(usu.NombreUsuario);

                tema.IdUsuario = usu2.IdUsuario;
                tema.FechaHoraTema = DateTime.Now;
                tema.DescripcionTema = Server.HtmlEncode(tema.DescripcionTema);
                string idVideo = null;
                string urlYoutube = null;
                if (tema.UrlYoutube != null)
                {
                    idVideo = Regex.Match(tema.UrlYoutube, @"(?:youtube\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&amp;]v=)|youtu\.be\/)([^""&amp;?\/ ]{11})").Groups[1].Value;
                    urlYoutube = "http://www.youtube.com/embed/" + idVideo;
                }

                if (usu.IdRol != 0)
                {
                    if (ModelState.IsValid)
                    {
                        TemaService.NuevoTema(new RepositorioClases.Tema()
                        {
                            IdCiudad = tema.IdCiudad,
                            DescripcionTema = Server.HtmlEncode(tema.DescripcionTema),
                            FechaHoraTema = tema.FechaHoraTema,
                            TituloTema = tema.TituloTema,
                            IdUsuario = tema.IdUsuario,
                            IdCategoria = tema.IdCategoria,
                            ContenidoTema = Server.HtmlEncode(tema.ContenidoTema),
                            UrlYoutube = urlYoutube,
                            IdGaleria = tema.IdGaleria
                        });
                    }
                    return RedirectToAction("MisTemas");
                }
                else
                {
                    return RedirectToAction("Error", "General");
                }
            }
            else
            {
                return RedirectToAction("Error", "General", new { mensaje = "Debe loguearse para acceder a esta sección" });
            }
        }

        public ActionResult EditarTema(int IdTema)
        {
            Usuario usu = (Usuario)(Session["Usuario"]);
            if (usu != null)
            {
                Session["IdTema"] = IdTema;
                List<Tema> temas = TemaService.ObtenerTemaPorIdTema(IdTema);
                TemaModel tema = new TemaModel();

                tema.DescripcionTema = Server.HtmlDecode(temas[0].DescripcionTema);
                tema.IdCiudad = temas[0].IdCiudad;
                tema.TituloTema = temas[0].TituloTema;
                tema.ContenidoTema = Server.HtmlDecode(temas[0].ContenidoTema);
                tema.IdCategoria = temas[0].IdCategoria;
                tema.IdGaleria = temas[0].IdGaleria;
                tema.UrlYoutube = temas[0].UrlYoutube;
                
                List<Paises> paises = PaisServicio.ObtenerPaises();
                ViewBag.paises = paises;
                int? idciudad = tema.IdCiudad;
                if (idciudad == null)
                {
                    idciudad = 0;
                }
                Paises pais = PaisServicio.ObtenerPaisPorIdCiudad(idciudad);
                ViewBag.paisseleccionado = pais.PaisCodigo;
                ViewBag.ciudadseleccionada = idciudad;

                List<Ciudades> ciudades = CiudadServicio.ObtenerCiudades();
                ViewBag.ciudades = ciudades;

                List<CategoriasTemas> categorias = TemaService.ObtenerCategorias(null);
                ViewBag.categorias = categorias;

                List<Galeria> galerias = GaleriaService.ObtenerGaleria(usu.IdUsuario);
                ViewBag.galerias = galerias;

                return View(tema);
            }
            else
            {
                return RedirectToAction("Error", "General", new { mensaje = "Debe loguearse para acceder a esta sección" });
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditarTema(TemaModel tema)
        {
            Usuario usu = (Usuario)(Session["Usuario"]);
            if (usu != null)
            {
                int idTema = (int)(Session["IdTema"]);
                Usuario usu2 = UsuarioServicio.ObtenerUsuarioPorNombre(usu.NombreUsuario);

                string idVideo = null;
                string urlYoutube = null;
                if (tema.UrlYoutube != null)
                {
                    idVideo = Regex.Match(tema.UrlYoutube, @"(?:youtube\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&amp;]v=)|youtu\.be\/)([^""&amp;?\/ ]{11})").Groups[1].Value;
                    urlYoutube = "http://www.youtube.com/embed/" + idVideo;
                }

                tema.IdTema = idTema;
                tema.DescripcionTema = Server.HtmlEncode(tema.DescripcionTema);
                tema.IdUsuario = usu2.IdUsuario;
                tema.FechaHoraTema = TemaService.ObtenerTemaPorIdTema(idTema)[0].FechaHoraTema;
                tema.FechaEliminacion = null;
                tema.ContenidoTema = Server.HtmlDecode(tema.ContenidoTema);
                tema.IdCategoria = tema.IdCategoria;
                tema.UrlYoutube = urlYoutube;
                tema.IdGaleria = tema.IdGaleria;
                
                if (usu.IdRol != 0)
                {
                    if (ModelState.IsValid)
                    {
                        TemaService.EditarTema(new RepositorioClases.Tema()
                        {
                            IdCiudad = tema.IdCiudad,
                            DescripcionTema = tema.DescripcionTema,
                            FechaHoraTema = tema.FechaHoraTema,
                            TituloTema = tema.TituloTema,
                            IdUsuario = tema.IdUsuario,
                            IdCategoria = tema.IdCategoria,
                            ContenidoTema = tema.ContenidoTema,
                            IdTema = tema.IdTema,
                            IdGaleria=tema.IdGaleria,
                            UrlYoutube=tema.UrlYoutube
                        });
                    }
                    return RedirectToAction("MisTemas");
                }
                else
                {
                    return RedirectToAction("Error", "General");
                }
            }
            else
            {
                return RedirectToAction("Error", "General", new { mensaje = "Debe loguearse para acceder a esta sección" });
            }
        }

        public ActionResult EliminarTema(int IdTema)
        {
            Usuario usu = (Usuario)(Session["Usuario"]);
            if (usu != null)
            {
                Session["IdTema"] = IdTema;

                TemaService.EliminarTema(IdTema);

                return RedirectToAction("MisTemas");
            }
            else
            {
                return RedirectToAction("Error", "General", new { mensaje = "Debe loguearse para acceder a esta sección" });
            }
        }

        public ActionResult MostrarTema(int IdTema)
        {
            Usuario usu = (Usuario)(Session["Usuario"]);
            List<Tema> temas = TemaService.ObtenerTemaPorIdTema(IdTema);
            TemaModel tema = new TemaModel();

            tema.IdUsuario = temas[0].IdUsuario;
            tema.NombreUsuarioTema = ComentarioService.ObtenerUsuarioPorIdUsuario(tema.IdUsuario).NombreUsuario;
            tema.DescripcionTema = Server.HtmlDecode(temas[0].DescripcionTema);
            tema.IdCiudad = temas[0].IdCiudad;
            tema.TituloTema = temas[0].TituloTema;
            tema.ContenidoTema = Server.HtmlDecode(temas[0].ContenidoTema);
            tema.IdCategoria = temas[0].IdCategoria;
            tema.IdTema = temas[0].IdTema;
            tema.IdGaleria = temas[0].IdGaleria;
            tema.UrlYoutube = temas[0].UrlYoutube;

            List<Comentario> comentarios = TemaService.ObtenerComentariosPorTema(temas[0].IdTema);
            List<ComentarioModel> comenList = new List<ComentarioModel>();
            foreach (var cm in comentarios)
            {
                ComentarioModel comen = new ComentarioModel();
                string Nombre = ComentarioService.ObtenerUsuarioPorIdUsuario(cm.IdUsuario).NombreUsuario;
                comen.NombreUsuario = Nombre;
                comen.FechaHoraComentario = cm.FechaHoraComentario;
                comen.DescripcionComentario = cm.DescripcionComentario;
                comen.FechaHoraEliminacion = cm.FechaHoraEliminacion;
                comen.IdComentario = cm.IdComentario;
                comen.IdTema = cm.IdTema;
                comen.IdUsuario = cm.IdUsuario;
                if (usu != null)
                {
                    if (cm.IdUsuario == usu.IdUsuario)
                    {
                        comen.ComentarioEditable = true;
                        comen.PermitirDenuncia = false;
                    }
                    else
                    {
                        comen.ComentarioEditable = false;
                        if ((DenunciasComentariosService.ObtenerDenuncia(usu.IdUsuario, cm.IdComentario).Count) == 0)
                        {
                            comen.PermitirDenuncia = true;
                        }
                        else
                        {
                            comen.PermitirDenuncia = false;
                        }
                    }

                }
                else
                {
                    comen.ComentarioEditable = false;
                    comen.PermitirDenuncia = false;
                }

                comenList.Add(comen);
            }
            tema.Comentarios = comenList;


            ViewBag.ciudad = UsuarioServicio.ObtenerCiudadPorIdCiudad(tema.IdCiudad).CiudadNombre;
            ViewBag.categoria = TemaService.ObtenerCategorias(tema.IdCategoria)[0].DescripcionCategoria;

            return View(tema);
        }

        public JsonResult CompartirFacebook(int idTemaComFace, string estado)
        {
            try
            {
                dynamic result;

                FacebookClient client = new FacebookClient();//ConfigurationManager.AppSettings["FacebookAppToken"]);
                Usuario usu = (Usuario)(Session["Usuario"]);
                var accestokenusu = "";
                if (usu.IdFacebook != null)
                {
                    accestokenusu = UsuarioServicio.ObtenerUsuarioFacebookPorId(usu.IdFacebook).AccessToken;

                }

                result = client.Get("oauth/access_token", new
                {
                    client_id = "1610450629193478",
                    client_secret = "98ac7d25614e680b6d03948a6f3e96a4",
                    grant_type = "client_credentials",
                    scope = "publish_stream, publish_actions",
                    permissions = "publish_stream, publish_actions"
                });
                client.AccessToken = accestokenusu;

                var parametros = new Dictionary<string, object>();
                parametros["message"] = estado;
                parametros["link"] = "http://experienciaturistica.apphb.com/Tema/MostrarTema?IdTema=" + idTemaComFace;

                result = client.Post("/me/links", parametros);
                result = client.Get(usu.IdFacebook);
                return Json("Ok", JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json("No", JsonRequestBehavior.AllowGet);
            }
        }
        
        
        public ActionResult PostStatus()
        {
            
            Usuario usu = (Usuario)(Session["Usuario"]);
            var nombre = UsuarioServicio.ObtenerUsuarioTwitterPorId(usu.IdTwitter).UserName;
            var acctoken = UsuarioServicio.ObtenerUsuarioTwitterPorId(usu.IdTwitter).AccessToken;

            TwitterCredentials.SetCredentials("467140253-8swK3rskWPPcfGv4BKmvVNFM9rNie5BBCTAnlGCj", "vawVjjQgyOda1G3ni9O5uo2vYseQ3g83qA1F8O6xGEW6K", "u2dT9aOyGxUzoYTEbwFRgq0gR", "5lB5FMxHc3jbOVYKRGKe0nwDroQoYIgBVIGgXYPhSIOXuyyOVy");
            var credentials = TwitterCredentials.CreateCredentials("467140253-8swK3rskWPPcfGv4BKmvVNFM9rNie5BBCTAnlGCj", "vawVjjQgyOda1G3ni9O5uo2vYseQ3g83qA1F8O6xGEW6K", "u2dT9aOyGxUzoYTEbwFRgq0gR", "5lB5FMxHc3jbOVYKRGKe0nwDroQoYIgBVIGgXYPhSIOXuyyOVy");
            TwitterCredentials.ExecuteOperationWithCredentials(credentials, () =>
            {
                Tweet.PublishTweet("Probandooooooo");
            });



            return View();
        }


        [HttpPost]
        public JsonResult GetCiudadbyPaisCodigo(string PaisCodigo)
        {
            return Json(new SelectList(CiudadServicio.ObtenerCiudades().Where(x => x.PaisCodigo == PaisCodigo).ToList(), "CiudadId", "CiudadNombre"));
        }

        public ActionResult MostrarTemasPorCategoria(int IdCategoria)
        {
            List<Tema> temasClase = TemaService.ObtenerTemasPorIdCategoria(IdCategoria);
            List<TemaModel> temasModel = new List<TemaModel>();
            foreach (var tem in temasClase)
            {
                TemaModel tema = new TemaModel();
                tema.FechaHoraTema = tem.FechaHoraTema;
                tema.DescripcionTema = Server.HtmlDecode(tem.DescripcionTema);
                tema.IdCiudad = tem.IdCiudad;
                tema.IdUsuario = tem.IdUsuario;
                tema.IdTema = tem.IdTema;
                tema.TituloTema = tem.TituloTema;
                tema.IdCategoria = tem.IdCategoria;
                tema.ContenidoTema = Server.HtmlDecode(tem.ContenidoTema);
                tema.IdGaleria = tem.IdGaleria;
                tema.UrlYoutube = tem.UrlYoutube;

                temasModel.Add(tema);
            }

            string nombreCategoria = TemaService.ObtenerCategorias(IdCategoria)[0].DescripcionCategoria;

            List<CategoriasTemas> categorias = TemaService.ObtenerCategorias(null);
            ViewBag.listaCategorias = categorias;

            if (temasModel.Count != 0)
            {
                ViewBag.Mensaje = "Temas en categoria: " + nombreCategoria;
            }
            else
            {
                ViewBag.Mensaje = "No existen temas disponibles para la categoria: " + nombreCategoria;
            }

            return View(temasModel);
        }

    }
}
