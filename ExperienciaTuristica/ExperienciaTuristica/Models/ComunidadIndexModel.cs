﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExperienciaTuristica.Models
{
    public class ComunidadIndexModel
    {
        public List<TemaModel> temas { get; set; }
        public List<GaleriaModel> galerias { get; set; }
        public SocialUserModel socialUser { get; set; }
    }
}