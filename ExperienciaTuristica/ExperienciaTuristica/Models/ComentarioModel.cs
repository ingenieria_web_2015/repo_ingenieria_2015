﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExperienciaTuristica.Models
{
    public class ComentarioModel
    {
        public int IdTema { get; set; }

        public int IdComentario { get; set; }

        public int IdUsuario { get; set; }

        public DateTime FechaHoraComentario { get; set; }

        public string DescripcionComentario { get; set; }

        public bool? ComentarioBloqueado { get; set; }

        public DateTime? FechaHoraEliminacion { get; set; }

        public string NombreUsuario { get; set; }

        public string FotoPerfil { get; set; }

        public bool? ComentarioEditable { get; set; }

        public bool? PermitirDenuncia { get; set; }
    }
}