﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExperienciaTuristica.Models
{
    public class BusquedaAvanzadaModel
    {
        public string titulo { get; set; }
        public string copete { get; set; }
        public string descripcion { get; set; }
        public string comentario { get; set; }
    }
}