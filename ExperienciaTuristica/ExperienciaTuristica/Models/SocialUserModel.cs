﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExperienciaTuristica.Models
{
    public class SocialUserModel
    {
        public string IdFacebook { get; set; }
        public string IdTwitter { get; set; }
    }
}