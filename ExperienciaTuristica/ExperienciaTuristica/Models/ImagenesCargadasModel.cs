﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExperienciaTuristica.Models
{
    public class ImagenesCargadasModel
    {
        public int IdGaleria { get; set; }
        public int IdImagen { get; set; }
        public string DescripcionImagen { get; set; }
        public DateTime FechaHoraImagen { get; set; }
        public string PathImagen { get; set; }
    }
}