﻿using RepositorioClases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExperienciaTuristica.Models
{
    public class ImagenesModel
    {
        public IEnumerable<HttpPostedFileBase> archivos { get; set; }
        public int IdGaleria { get; set; }
        public IEnumerable<Imagen> imagenesCargadas { get; set; }
    }
}