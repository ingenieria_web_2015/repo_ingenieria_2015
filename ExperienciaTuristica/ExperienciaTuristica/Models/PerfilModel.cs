﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ExperienciaTuristica.Models
{
    public class PerfilModel
    {
        public int IdUsuario { get; set; }

        [Required]
        [Display(Name = "Nombre de usuario")]
        public string NombreUsuario { get; set; }


        [Required]
        [Display(Name = "Nombre y apellido")]
        public string NombreApellido { get; set; }

        [Required]
        [Display(Name = "Mail")]
        public string Mail { get; set; }

        public int? IdCiudad { get; set; }
        public SelectList Ciudades { get; set; }

        [Display(Name = "Nombre Ciudad")]
        public string NombreCiudad { get; set; }

        [Display(Name = "País")]
        public string PaisCodigo { get; set; }

        [Required]
        [Display(Name = "Dirección")]
        public string Direccion { get; set; }

        [Display(Name = "Fecha Nacimiento")]
        public DateTime? FechaNacimiento { get; set; }

        [Display(Name = "Foto perfil")]
        public string FotoPerfil { get; set; }
    }
}