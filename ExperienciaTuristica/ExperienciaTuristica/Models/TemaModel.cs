﻿using Datos;
using RepositorioClases;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ExperienciaTuristica.Models
{
    public class TemaModel
    {
        public int IdTema { get; set; }

        [Required]
        [Display(Name = "Título Tema")]
        public string TituloTema { get; set; }

        [Display(Name = "Nombre Ciudad")]
        public string NombreCiudad { get; set; }

        [Required]
        [Display(Name = "Nombre de usuario")]
        public int IdUsuario { get; set; }

        [Required]
        [Display(Name = "Fecha Creación")]
        public System.DateTime FechaHoraTema { get; set; }

        [Required]
        [Display(Name = "Contenido del tema")]
        public string ContenidoTema { get; set; }

        [Required]
        [Display(Name = "Descripción del tema")]
        public string DescripcionTema { get; set; }


        [Display(Name = "País")]
        public string PaisCodigo { get; set; }

        [Required]
        [Display(Name = "Ciudad")]
        public int IdCiudad { get; set; }
        public SelectList Ciudades { get; set; }

        [Required]
        [Display(Name = "Categoria")]
        public int IdCategoria { get; set; }
        public SelectList Categorias { get; set; }

        [Display(Name = "Fecha de eliminación")]
        public DateTime? FechaEliminacion { get; set; }

        public List<ComentarioModel> Comentarios { get; set; }

        public string NombreUsuarioTema { get; set; }

        [Display(Name = "Video de youtube")]
        public string UrlYoutube { get; set; }

        [Display(Name = "Galeria relacionada")]
        public int? IdGaleria { get; set; }

    }
}