﻿using Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace ExperienciaTuristica.Models
{
    public class CustomRoleProvider : RoleProvider
    {
        public override string[] GetRolesForUser(string username)
        {
            using (Entities context = new Entities())
            {
                Usuarios user = context.Usuarios.FirstOrDefault(u => u.NombreUsuario.Equals(username, StringComparison.CurrentCultureIgnoreCase) || u.Mail.Equals(username, StringComparison.CurrentCultureIgnoreCase));

                var roles = from r in context.Roles
                            where user.IdRol == r.IdRol
                            select r.DescripcionRol;
                if (roles != null)
                    return roles.ToArray();
                else
                    return new string[] { }; ;
            }
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            using (Entities context = new Entities())
            {
                Usuarios user = context.Usuarios.FirstOrDefault(u => u.NombreUsuario.Equals(username, StringComparison.CurrentCultureIgnoreCase) || u.Mail.Equals(username, StringComparison.CurrentCultureIgnoreCase));

                var roles = from r in context.Roles
                            where user.IdRol == r.IdRol
                            select r.DescripcionRol;
                if (roles != null)
                    return roles.Any(r => r.Equals(roleName, StringComparison.CurrentCultureIgnoreCase));
                else
                    return false;
            }
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override string ApplicationName
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new NotImplementedException();
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }
    }
}