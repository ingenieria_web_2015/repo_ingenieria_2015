﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ExperienciaTuristica.Models
{
    public class CaptchaModel
    {
        [Required]
        [Display(Name = "Cuanto es?:")]
        public string Captcha { get; set; }

        public int id { get; set; }
    }
}