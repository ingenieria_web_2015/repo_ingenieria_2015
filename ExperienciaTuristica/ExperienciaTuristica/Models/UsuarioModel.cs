﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Servicios;
using Datos;
using System.Web.Mvc;

namespace ExperienciaTuristica.Models
{
    public class UsuarioModel
    {
        public int IdUsuario { get; set; }

        [Required]
        [Display(Name = "Nombre de usuario")]
        public string NombreUsuario { get; set; }

        [Required]
        [Display(Name = "Nombre y apellido")]
        public string NombreApellido { get; set; }

        [Required]
        [Display(Name = "Mail")]
        public string Mail { get; set; }

        [Required]
        [Display(Name = "Contraseña")]
        public string Contraseña { get; set; }

        [Required]
        [Display(Name = "Rol")]
        public int IdRol { get; set; }
        //public SelectList Roles { get; set; }

        [Display(Name = "País")]
        public string PaisCodigo { get; set; }
        //public SelectList PaisLista { get; set; }

        [Display(Name="Ciudad")]
        public int? IdCiudad { get; set; }
        //public SelectList CiudadesLista { get; set; }

        [Required]
        [Display(Name = "Dirección")]
        public string Direccion { get; set; }

        [Display(Name = "Fecha Nacimiento")]
        public DateTime? FechaNacimiento { get; set; }

        [Display(Name = "Nombre Ciudad")]
        public string NombreCiudad { get; set; }

        [Display(Name = "Descripción Estado")]
        public string DescripcionBloqueo { get; set; }

        public int? IdEstado { get; set; }
        public SelectList Estados { get; set; }

        public string FotoPerfil { get; set; }

        public string IdFacebook { get; set; }

        public string IdTwitter { get; set; }

        [Display(Name = "Descripción Rol")]
        public string DescripcionRol { get; set; }

        public List<Rol> Roles
        {
            get
            {
                return RolServicio.GetRole(null).Select(r => new Rol()
                {
                    DescripcionRol = r.DescripcionRol,
                    IdRol = r.IdRol
                }).ToList();
            }

            set { }
        }

    }
    public class Rol
    {
        public int IdRol { get; set; }

        public string DescripcionRol { get; set; }
    }


}