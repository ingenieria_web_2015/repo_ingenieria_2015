﻿using RepositorioClases;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ExperienciaTuristica.Models
{
    public class GaleriaModel
    {
        public int IdGaleria { get; set; }

        [Required]
        [Display(Name = "Nombre de usuario")]
        public int IdUsuario { get; set; }

        [Display(Name = "Nombre Ciudad")]
        public string NombreCiudad { get; set; }

        [Required]
        [Display(Name = "Fecha Creación")]
        public DateTime FechaHoraGaleria { get; set; }

        [Required]
        [Display(Name = "Título de la galería")]
        public string TituloGaleria { get; set; }

        [Required]
        [Display(Name = "Descripción de la galería")]
        public string DescripcionGaleria { get; set; }

        [Display(Name = "País")]
        public string PaisCodigo { get; set; }

        [Required]
        [Display(Name="Ciudad")]
        public int IdCiudad { get; set; }
        public SelectList Ciudades { get; set; }

        [Display(Name = "Fecha de eliminación")]
        public DateTime? FechaEliminacion { get; set; }

        public IEnumerable<Imagen> imagenesCargadas { get; set; }
    }
}