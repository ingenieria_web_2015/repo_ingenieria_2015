﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExperienciaTuristica.Models
{
    public class DenunciaModel
    {
        public int IdUsuario { get; set; }

        public int IdComentario { get; set; }

        public string MotivoDenuncia { get; set; }

        public DateTime FechaHoraDenuncia { get; set; }
        

    }
}