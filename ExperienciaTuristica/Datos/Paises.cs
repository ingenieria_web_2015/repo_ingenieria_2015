//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Datos
{
    using System;
    using System.Collections.Generic;
    
    public partial class Paises
    {
        public Paises()
        {
            this.Ciudades = new HashSet<Ciudades>();
        }
    
        public string PaisCodigo { get; set; }
        public string PaisNombre { get; set; }
        public string PaisContinente { get; set; }
        public string PaisRegion { get; set; }
        public Nullable<int> PaisCapital { get; set; }
    
        public virtual ICollection<Ciudades> Ciudades { get; set; }
    }
}
